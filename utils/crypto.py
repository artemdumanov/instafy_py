import base64
from urllib.parse import quote, unquote


class QuoteEncryptr:
    # if rewrite key/quoted, u won't decode value
    __quoted = True
    __key = 'M<Mmifdsa&2#$M$T%#8293#$%^&Thjyg3)(Iuhed0q+_)9()*&^TR$EW#tvgb_)_+hjs'

    @staticmethod
    def encode(clear):
        enc = []
        for i in range(len(clear)):
            key_c = QuoteEncryptr.__key[i % len(QuoteEncryptr.__key)]
            enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
            enc.append(enc_c)
        encoded = base64.urlsafe_b64encode("".join(enc).encode()).decode()

        return quote(encoded) if QuoteEncryptr.__quoted else encoded

    @staticmethod
    def decode(enc):
        dec = []
        enc = unquote(enc) if QuoteEncryptr.__quoted else enc
        enc = base64.urlsafe_b64decode(enc).decode()
        for i in range(len(enc)):
            key_c = QuoteEncryptr.__key[i % len(QuoteEncryptr.__key)]
            dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
            dec.append(dec_c)
        return "".join(dec)
