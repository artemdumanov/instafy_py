import logging

from main.settings import DEBUG


def getLogger():
    return logging.getLogger('django')
