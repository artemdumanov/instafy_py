import json
import random
from datetime import datetime, timedelta
from time import sleep

import requests
from django.db.models import Q
from django.utils import timezone

from models.models import ProxyForRefund, Proxy, InstagramAccount
from utils import logging
from utils.manager import mail_admin

log = logging.getLogger()


class ProxyManager():
    __api_key = 'b15d36c662-28ec7abcf5-a455a56e55'
    __url = 'https://proxy6.net'
    lock = False

    def __init__(self):
        if not ProxyManager.lock:
            # prolong proxy for 3 days on 00:00,
            # if it expires today and not trial
            # PySched(self.__prolong_expired(today=True))
            # only when offline
            # PySched(self.report(), hours=(9,))
            log.info('Init proxy manager')

            ProxyManager.lock = True

    def request_api(self, method, params):
        url = '{}/api/{}/{}'.format(ProxyManager.__url,
                                    ProxyManager.__api_key,
                                    method)
        log.debug('Send request to proxy API {} with params {}'
                  .format(url, params))
        try:
            req = requests.get(url, params=params)
        except Exception as e:
            log.exception('When request {} was occured an error '
                          .format(url))
            # TODO which to use??
            raise ProxyException()
        else:
            return req

    @staticmethod
    def check_errors(resp, prefix='', postfix=''):
        """
        https://proxy6.net/developers Error codes
        :param resp: response of request
        :param prefix: optional. prepend error, when error occurred
        :param postfix: optional. append error, when error occurred
        :return: True if errors
        """
        log.debug('Check errors for response {}'.format(resp))
        if resp['status'] == 'no':
            raise ProxyException('{} ERROR: {} {} {}'.format(prefix,
                                                             resp['error_id'],
                                                             resp['error'],
                                                             postfix))
        else:
            return False

    def buy(self, account, ip, period, account_type, version=6, proxy_type='http', amend_proxy=None):
        """
        Buy proxy
        :param account_type:
        :param account: Instagram account
        :param country:
        :param period:
        :param version: IPv6 IPv4
        :param proxy_type: http/socks
        :return: success or not
        """
        username = account.username
        # country = None
        # if amend_proxy is None:
        #     country = self.get_country(ip)
        # else:
        #     country = amend_proxy.country
        country = self.get_country(ip) if amend_proxy is None else amend_proxy.country
        params = {
            'count': 1,
            'period': period,
            'country': country,
            'version': version,
            'type': proxy_type
        }

        req = self.request_api('buy', params)
        if req is None or req.status_code != 200:
            log.exception('Can\'t buy proxy; API status != 200 or else {} {} {} {} {}'
                          .format(username, country, period, version, proxy_type))
            raise ProxyException()

        try:
            resp = req.json()
        except Exception as e:
            log.exception('Can\'t buy proxy; Can\'t serialize json {} {} {} {} {}'
                          .format(username, country, period, version, proxy_type))
            raise ProxyException()
        else:
            if self.check_errors(resp=resp,
                                 prefix='When trying to buy proxy occurred',
                                 postfix='{} {} {} {} {}'
                                         .format(username, country, period, version, proxy_type)):
                return False
            else:
                proxies_list = resp['list']
                if len(proxies_list) > 1:
                    log.warning('When trying to buy proxy. '
                                'Return more than two proxies {} {} {} {} {} {}'
                                .format(proxies_list, username,
                                        country, period,
                                        version, proxy_type))

                proxies_info = next(iter(proxies_list.values()))

                proxy = self.parse(proxies_info['user'], proxies_info['pass'],
                                   proxies_info['host'], proxies_info['port'])
                proxy_provider_id = proxies_info['id']

                price = resp['price']
                currency = resp['currency']
                now = datetime.now()
                proxy_provider_id = int(proxy_provider_id)
                meta_key = now.strftime("%Y/%m/%d")
                meta_time = now.strftime("%H:%M:%S")
                meta = {
                    meta_key: {
                        'account_type': account_type,
                        'proxy': proxy,
                        'country': country,
                        'currency': currency,
                        'price': price,
                        'proxy_type': proxy_type,
                        'time': meta_time
                    }
                }
                meta = json.dumps(meta)
                log.info('Buy proxy {} provider id {} for {} {} '
                         'for {} days {} {} cost {} {}'
                         .format(proxy, proxy_provider_id,
                                 username, country,
                                 period, version,
                                 proxy_type, price,
                                 currency))
                if self.check(proxy):
                    if amend_proxy is None:
                        return self._save(account, proxy,
                                          proxy_provider_id, period,
                                          country, timezone.now(),
                                          price, meta,
                                          account_type, ip)
                    else:
                        return self._amend(account, amend_proxy,
                                           proxy, proxy_provider_id,
                                           period, timezone.now(),
                                           price, account_type, meta)
                else:
                    log.info('Buy proxy {} check was FAILED provider id {} for {} {} '
                             'for {} days {} {} cost {} {}'
                             .format(proxy, proxy_provider_id,
                                     username, country,
                                     period, version,
                                     proxy_type, price,
                                     currency))
                    self.request_refund(account, proxy,
                                        proxy_provider_id, country,
                                        period, json.dumps(meta))
                    # recursion. buy new -> ?bad -> refund -> buy new
                    self.buy(account, country, period, account_type, version, proxy_type)

    @staticmethod
    def _save(account, proxy,
              proxy_provider_id, period,
              country, start_date,
              price, meta,
              account_type, ip):
        try:
            period = int(period)
            end_date = start_date + timedelta(days=period)
            prx = Proxy()
            prx.account = account
            prx.proxy = proxy
            prx.proxy_provider_id = proxy_provider_id
            prx.period = period
            prx.country = country
            prx.start_date = start_date
            prx.end_date = end_date
            prx.price = price
            prx.meta = meta
            prx.ip = ip
            prx.save()

            payment = [start_date.strftime('%Y/%m/%d'),
                       end_date.strftime("%Y/%m/%d")]
            history = account.history_of_payments if account.history_of_payments is not None else '[]'
            payments = json.loads(history)
            payments.append(payment)
            account.history_of_payments = json.dumps(payments)
            account.account_type = account_type

            print('\n')
            print('Save account {}'.format(prx))
            print('payment: {} type {}'.format(payments, type(payments)))
            print('account type: {} type {}'.format(account_type, type(account_type)))
            print('\n')

            account.save(update_fields=['history_of_payments', 'account_type'])
            log.debug('Update account {}'.format(account))
        except Exception:
            raise ProxyException('Exception, when trying to save proxy {} {} for {} '
                                 'date {} price {} meta {} '
                                 .format(proxy, country, account.username,
                                         start_date, price, meta))
        else:
            log.info('Save new proxy {} {} for {} from {} to {} price {} meta {} '
                     .format(proxy, country, account.username,
                             start_date, end_date, price, meta))
            return True

    def _amend(self, account,
               proxy, proxy_str,
               proxy_provider_id, period,
               start_date, price,
               account_type, meta):
        try:
            period = int(period)
            end_date = start_date + timedelta(days=period)

            payment = [start_date.strftime('%Y/%m/%d'),
                       end_date.strftime("%Y/%m/%d")]
            history = account.history_of_payments if account.history_of_payments \
                                                     is not None else '[]'
            payments = json.loads(history)
            payments.append(payment)

            account.history_of_payments = json.dumps(payments)
            account.account_type = account_type

            account.save(update_fields=['history_of_payments', 'account_type'])
            log.debug('Amend account {}'.format(account))

            proxy.proxy = proxy_str
            proxy.proxy_provider_id = proxy_provider_id
            proxy.period = period
            # = or += ?
            proxy.price = price
            proxy.meta = meta

            proxy.save(update_fields=['proxy', 'proxy_provider_id',
                                      'period', 'price',
                                      'meta'])
            log.debug('Amend proxy {}'.format(proxy))
        except Exception:
            raise ProxyException('Exception, when trying to amend proxy {} for {} '
                                 .format(proxy, account))
        else:
            log.info('Amend proxy {} {}'.format(proxy, account))
            return True

    # TODO check three times
    @staticmethod
    def check(proxy):
        """
        :param proxy:
        :return: False if proxy invalid
        """
        log.debug('Trying to check proxy {}'.format('proxy'))
        try:
            prxs = {
                'http': 'http://{}'.format(proxy),
                'https': 'http://{}'.format(proxy),
            }
            sleep(15)
            r = requests.get('https://google.com', proxies=prxs)

            if r.status_code != 200:
                log.exception('Proxy check {} was failed '
                              .format(proxy))
                raise ProxyException()
        except Exception as e:
            log.exception('Proxy check {} was failed '
                          .format(proxy), e)
            raise ProxyException()
        else:
            log.debug('Proxy {} check is successful'.format(proxy))
            return True

    @staticmethod
    def request_refund(instagram, proxy,
                       proxy_provider_id, country,
                       period, start_date=None,
                       finish_date=timezone.now(), meta=None):
        """
        :param proxy_provider_id:
        :param period:
        :param country:
        :param meta: meta for refund proxy
        :param start_date: proxy start date. optional
        :param finish_date: proxy start date. default today
        :param instagram: instagram account class, not username
        :param proxy: string, Not class (!)
        :return:
        """
        refund = ProxyForRefund()
        refund.proxy = proxy
        refund.proxy_provider_id = proxy_provider_id
        refund.country = country
        refund.period = period
        refund.account = instagram
        refund.start_date = start_date
        refund.finish_date = finish_date
        refund.meta = meta
        refund.save()

        message = 'Request refund of proxy {} {} for account {} on {} days' \
                  'start: {} finish: {} meta: {}' \
            .format(proxy, country, instagram, period,
                    start_date, finish_date, meta)

        log.warning(message)

        mail_admin(message, subject='New proxy refund')

    def prolongs(self, instagrams, account_type, period=30):
        """
        :param account_type:
        :param instagrams: QuerySet of Instagrams
        :param period: days for prolong
        :return:
        """
        for instagram in instagrams:
            self.prolong(instagram, account_type, period)

    def prolong(self, instagram, account_type, period):
        username = instagram.username
        # shit code filter --> get
        proxy = Proxy.objects.filter(account=instagram)

        if not proxy.exists():
            raise ProxyException('No proxy associated with account {}'
                                 .format(username))
        proxy = proxy.first()
        log.debug('Trying to prolong proxy {} of {} basic {} on {}'
                  .format(proxy.proxy, username, account_type, period))
        end_date = proxy.end_date
        dif = timezone.now() - end_date
        if dif.days >= 2:
            self.buy(instagram, proxy.ip,
                     period, account_type,
                     amend_proxy=proxy)
        else:
            id = proxy.proxy_provider_id
            params = {
                'ids': id,
                'period': period
            }
            req = self.request_api('prolong', params)
            status = req.status_code
            if status != 200:
                raise ProxyException('Can\'t prolong proxy id: {}; API status != 200 for {} '
                                     'on {} days; STATUS {}'.format(id, username, period, status))

            try:
                resp = req.json()
            except Exception as e:
                msg = 'Can\'t prolong proxy id: {}; Can\'t serialize json for {} ' \
                      'on {} days'.format(id, username, period)
                log.exception(msg)
                raise ProxyException(msg)
            else:
                if self.check_errors(resp=resp,
                                     prefix='When trying to prolong proxy occurred',
                                     postfix='{} {} {}'
                                             .format(id, username, period)):
                    return False
                else:
                    # self.low_balance(resp)
                    lst = resp['list']
                    items = lst.items()
                    if len(items) > 2:
                        log.warning('Get more than one proxies, when trying to prolong '
                                    '{} {} {} list: {}'.format(id, username,
                                                               period, items))
                    for key, data in items:
                        if int(data['id']) == id:
                            price = resp['price']

                            start_date = proxy.start_date
                            end_date = data['date_end']

                            expired_date = datetime.strptime(
                                end_date, "%Y-%m-%d %H:%M:%S"
                            )

                            instagram.account_type = account_type
                            instagram.expired_date = expired_date

                            instagram.save(update_fields=['account_type',
                                                          'expired_date'])

                            log.info('Update account type of {} type {}'
                                     .format(username, account_type,
                                             start_date, end_date, price))
                            return self.update(proxy, account_type,
                                               start_date, end_date,
                                               price, is_prolong=True)

    @staticmethod
    def update(proxy, account_type, start_date, end_date, price, is_prolong=False):
        """
        Update proxy
        :param proxy:
        :param account_type:
        :param start_date:
        :param end_date:
        :param price:
        :param is_prolong: optional, just for logging(update/prolong)
        :return:
        """
        action = 'prolong' if is_prolong else 'update'
        try:
            proxy.start_date = start_date
            start_date = start_date.strftime("%Y/%m/%d")
            end_date = datetime.strptime(end_date, "%Y-%m-%d %H:%M:%S")
            proxy.end_date = end_date
            end_date = end_date.strftime("%Y/%m/%d")
            proxy.account_type = account_type

            now = datetime.now()
            meta_key = now.strftime("%Y/%m/%d")
            meta_time = now.strftime("%H:%M:%S")
            meta_value = {
                'proxy': proxy.proxy,
                'account_type': account_type,
                'time': meta_time,
                'start_date': start_date,
                'end_date': end_date,
                'price': price
            }
            meta = proxy.meta
            meta = json.loads(meta)
            meta[meta_key] = meta_value
            proxy.meta = json.dumps(meta)
            proxy.save(update_fields=['start_date', 'end_date', 'meta'])
            # log.debug('Update proxy {}'.format(proxy))
        except Exception as e:
            message = 'When trying to {} proxy {} id: {} type: {} from {} to {} ' \
                      'price: {} meta: {}, error was occurred.' \
                .format(action, proxy.proxy, proxy.id_proxy,
                        account_type, start_date, end_date,
                        price, meta)
            log.exception(message)
            mail_admin(message, subject='New proxy refund')

            raise ProxyException()
        else:
            log.info('Successfully {} proxy {} id: {} type: {} '
                     'from {} to {} price: {} meta: {}'
                     .format(action, proxy.proxy, proxy.id_proxy,
                             account_type, start_date, end_date,
                             price, meta))
            return True

    @staticmethod
    def parse(user, password, host, port):
        log.debug('Parse proxy from {} {} {} {}'.format(user, password,
                                                        host, port))
        return '{}:{}@{}:{}'.format(user, password, host, port)

    @staticmethod
    def expired(today=True, ignore_trial=True):
        """
        Return all expired proxies, that expires or today, or later
        :return:
        """
        log.info('Start expiry check. today={} and ignore trial={}'
                 .format(today, ignore_trial))
        if ignore_trial:
            instagrams = InstagramAccount.objects \
                .filter(~Q(account_type='trial'))
        else:
            instagrams = InstagramAccount.objects.all()
        for instagram in instagrams:
            payments = json.loads(instagram.history_of_payments)
            payments = sorted(payments,
                              key=lambda x: max(x[0], x[1]),
                              reverse=False)
            start, end = max(payments)
            end = datetime.strptime(end, '%Y/%m/%d').date()
            now = datetime.now().date()
            log.debug('Check payments of {} {} last is ({}, {})'
                      .format(instagram.username, payments,
                              start, end))
            if today:
                if end == now:
                    yl = [instagram,
                          Proxy.objects.get(instagram=instagram)]
                    log.debug('Yield expired proxy {} of {}'.format(*yl))
                    yield yl
            else:
                if end > now:
                    yl = [instagram,
                          Proxy.objects.get(instagram=instagram)]
                    log.debug('Yield expired proxy {} of {}'.format(*yl))
                    yield yl

    def __prolong_expired(self, today=True):
        """
        Prolong expired proxies, if not trial
        :param today:
        :return:
        """
        for instagram, proxy in self.expired(today):
            self.prolong([instagram], instagram.account_type, 3)

    def __check_request(self, req):
        """
        Check request. If error false else true
        :param req:
        :return: true if errors
        """
        if req is not None and req.json()['status'] is not None and not self.check_errors(req.json()):
            log.debug('Successful request to API {}'.format(req.text))

            try:
                resp = req.json()
            except Exception as e:
                log.exception('Can\'t parse json request to API {}')
                raise ProxyException()
            else:
                return resp
        else:
            log.exception('Invalid response or request to proxy api {}'
                          .format(req))
            raise ProxyException()

    def get_country(self, ip):
        if ip is not None:
            try:
                countries = self.request_api('getcountry',
                                             params={'version': 6})
            except Exception as e:
                log.exception('Something wrong with request getcountries to api ')
                raise ProxyException()
            else:
                if not self.__check_request(countries):
                    return False
                try:
                    # freegeoip so slow
                    req = requests.get('http://freegeoip.net/json/{}'.format(ip))
                except Exception as e:
                    log.exception('Can\'t request for ip info {}'.format(e))
                    raise ProxyException()
                else:
                    resp = req.json()
                    country = resp['country_code'].lower()
                    # self.low_balance(countries.json())

                    country_list = countries.json()['list']
                    if country in country_list and self.country_available(country):
                        log.debug('Get country {} from ip {}'.format(country, ip))
                        return country
                    else:
                        return self.random_country(country_list)

    def country_available(self, country):
        req = self.request_api('getcount', {'country': country})
        resp = self.__check_request(req)

        if resp is False:
            raise ProxyException('When request country {} yield invalid response'
                                 .format(country))
        else:
            count = float(resp['count'])
            is_available = count > 10
            if is_available:
                log.debug('Country {} available'.format(country))
                return True
            else:
                log.debug('Amount of proxy of country {} is so low'
                          .format(country))

    # TODO nearest country
    def random_country(self, countries):

        if countries is None or not countries:
            raise ProxyException('No random countries available or countries is None')

        country = random.choice(countries)

        if self.country_available(country):
            log.debug('Random country {} available'.format(country))
            return country
        else:
            log.debug('Random country {} not available'.format(country))
            self.random_country(countries.remove(country))

    @staticmethod
    def low_balance(resp):
        """
        True if balance is low
        :param resp:
        :param status_report:
        :return:
        """
        balance = resp['balance']
        if float(balance) < 500 and int(balance[1]) % 5:
            log.warning('Proxy balance is low {}'.format(balance))
            mail_admin('Proxy balance is {}'.format(balance),
                       'Proxy balance is {}. '
                       'Please, put money to proxy account'.format(balance))
            return True
        else:
            return False

    def report(self):
        req = self.request_api('getproxy', {})
        resp = req.json()
        balance = float(resp['balance'])
        status = 'good' if balance > float(1000) else 'low'
        # TODO daily digest registrations
        log.debug('Balance is {}, status {}'.format(balance, status))
        mail_admin(subject='Proxy balance is {}'.format(status),
                   message='{} proxy balance status is {}'
                   .format(balance, status))

    @staticmethod
    def add_meta(kwargs, meta={}):
        now = datetime.now()
        meta_key = now.strftime("%Y/%m/%d")
        meta_time = now.strftime("%H:%M:%S")
        meta_value = {
            'time': meta_time
        }
        for key, val in kwargs:
            meta_value[key] = val
        meta[meta_key] = meta_value
        log.debug('Modify meta {}'.format(meta))

        return meta


class ProxyException(Exception):
    def __init__(self, message=''):
        super(Exception, self).__init__(message)
