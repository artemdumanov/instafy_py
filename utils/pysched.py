from datetime import datetime
from multiprocessing import Process
from time import sleep


class PySched:
    def __init__(self, func, kwargs={}, hours=(0,), sleep_time=60*60):
        p = Process(target=self.__make_target,
                    args=[func, kwargs, hours, sleep_time])
        p.start()

    @staticmethod
    def __make_target(func, kwargs, hours, sleep_time):
        while True:
            now = datetime.now()
            for hour in hours:
                if now.hour == hour:
                    func(**kwargs)
            sleep(sleep_time)