/**
 * Created by artem on 4/8/17.
 */

var isDlgOpen;

var app = angular.module('HeaderApp', ['ngMaterial', 'ngAnimate', 'ngAria', 'ngCookies', 'ngMessages',
    'ngMaterialSidemenu', 'pascalprecht.translate'])
    .controller('HeaderCtrl', function ($scope, $rootScope, $timeout, $mdSidenav, $log, $http, $cookies, $mdDialog, $q,
                                        $mdToast, $mdConstant, $window, $translate, $cookies) {
            $scope.language = (navigator.language || navigator.userLanguage);
            $scope.languages = ['en', 'ru'];
            $scope.language == undefined ? $scope.language = 'en' : $scope.language = $scope.language.substring(0, 2).toLowerCase();

            $scope.ruCountries = ['ru', 'rus', 'ua', 'ukr', 'bl', 'lt', 'lv', 'lav', 'be', 'bel', 'tk', 'tuk', 'et',
                'est', 'ky', 'kir', 'mo', 'mol', 'uz', 'uzb', 'az', 'aze', 'azj', 'azb', 'qxq', 'slq', 'aze', 'azj',
                'azb', 'qxq', 'slq', 'hy', 'hye', 'arm', 'hye'];
            (($scope.language != undefined) && ($scope.ruCountries.indexOf($scope.laguage) != -1)) ? $scope.language = 'ru' : $scope.language = 'en'
            $scope.getNameOfLanguage = function (lang) {
                var l = '';
                if (lang == 'en') l = "English";
                if (lang == 'ru') l = "Russian";
                return l
            };
            $translate.use($scope.language);
            var cookieLanguage = $cookies.get('language');
            if (cookieLanguage !== undefined) {
                $translate.use(cookieLanguage);
                $scope.language = cookieLanguage
            }
            $scope.setLanguage = function (lang) {
                $translate.use(lang);

                $http.post('/account/language/set', {language: lang}).then(function mySucces(response) {
                    if (response.status == '200') {
                        $translate('TOASTS.LANGUAGE.CHANGE_SUCCESSFUL').then(function (message) {
                            $cookies.put('language', lang);
                            $scope.language = lang;
                            $rootScope.showInfoToast(message)
                        }, function (translationId) {
                            $scope.headline = translationId;
                        });
                    }
                }, function myError(response) {
                    $rootScope.showReportToast()
                });
            };
            $scope.template = {};
            $scope.tooltips = true;
            $scope.separators = [$mdConstant.KEY_CODE.ENTER, $mdConstant.KEY_CODE.COMMA, $mdConstant.KEY_CODE.SPACE,
                $mdConstant.KEY_CODE.TAB, $mdConstant.KEY_CODE.SEMICOLON];

            $scope.separatorsOfTags = [$mdConstant.KEY_CODE.ENTER];
            $scope.comments = [];
            $scope.tags = [];
            $scope.tagBlackList = [];
            $scope.usernameBlacklist = [];
            $scope.isDashboardDisabled = false;
            $translate(['DASHBOARD.FOLLOWS.MODES.MODES.0', 'DASHBOARD.FOLLOWS.MODES.MODES.1',
                'DASHBOARD.FOLLOWS.MODES.MODES.2', 'DASHBOARD.FOLLOWS.MODES.MODES.3', 'DASHBOARD.FOLLOWS.MODES.MODES.4',
                'DASHBOARD.FOLLOWS.MODES.MODES.5'
            ]).then(function (translations) {
                function prefix(k) {
                    return [k, translations['DASHBOARD.FOLLOWS.MODES.MODES.' + k]]
                }

                // $scope.botModes = []
                // for (var i = 0; i <= 5; i++) $scope.botModes.push(prefix(i))
                $scope.botModes = [[0, 'Original'], [1, 'Modified'], [2, 'Unfollow who don\'t follow back'], [3, 'Follow base on recent feed.'], [4, 'Unfollow based on recent feed'], [5, 'Unfollow everybody']];

            }, function (translationIds) {
            });
            $scope.schedule = [['Mon', 9, 23],
                ['Tue', 9, 23],
                ['Wed', 9, 23],
                ['Thu', 9, 23],
                ['Fri', 9, 23],
                ['Sat', 9, 23],
                ['Sun', 9, 23]
            ];
            $scope.modeTip = function (index) {
                if (index == 0) return "Original mode.";
                if (index == 1) return "Modified mode. More efficient than original.";
                if (index == 2) return 'Unfollow who don\'t follow back mode.';
                if (index == 3) return 'Follow base on recent feed mode.';
                if (index == 4) return 'Unfollow based on recent feed mode.';
                if (index == 5) return 'Unfollow everybody. Don\'t use this mode rarely';

                return "Choose mode";
            };
            $scope.toggleLeft = buildDelayedToggler('left');

            $scope.checkInstagramAccounts = function () {
                $http.get('/accounts/').then(function mySucces(response) {
                    if (response.status == '200') {
                        $scope.instagramAccounts = response.data.accounts
                    }
                }, function myError(response) {
                    $rootScope.showReportToast()
                });
            };

            $scope.setCurrentAccount = function (account) {
                $http.post('/account/set/', {current_account: account}).then(function mySucces(response) {
                    if (response.status == '200') {
                        $scope.current_account = account;
                        $window.location.reload();
                    }
                }, function myError(response) {
                    $rootScope.showReportToast()
                });
            };

            setCurrentAccount = $scope.setCurrentAccount;

            $scope.addInstagram = function (ev) {

                $mdDialog.show({
                    controller: DialogController,
                    template: '<md-dialog aria-label="Add instagram account" style=" margin: 15px; max-width: 360px;width: 360px;">' +
                    '<form ng-cloak><md-toolbar><div class="md-toolbar-tools">' +
                    '<h2 style="margin-left: 22px">{{ \'ACCOUNT_MANAGEMENT.ADD_ACCOUNT\' | translate}}</h2>' +
                    '<span flex>' +
                    '</span><md-button class="md-icon-button" ng-click="cancel()" style="margin: 0 !important;padding: 0 !important;">' +
                    '<md-icon aria-label="Close dialog" >close</md-icon>' +
                    '</md-button></div></md-toolbar>' +
                    '<md-dialog-site_content><div class="md-dialog-site_content">' +
                    '<md-input-container class="md-block" style="margin: 0 !important;">' +
                    '<form name="addAccountForm">' +
                    '<div layout-gt-sm="column"><md-input-container class="md-block" flex-gt-sm="">' +
                    '<label>{{ \'ACCOUNT_MANAGEMENT.USERNAME\' | translate}}</label>' +
                    '<input name="username" ng-model="newAccount.username" required>' +
                    '<div class="hint" ng-show="showHints"></div><div ng-messages="newAccount.username.$error">' +
                    '<div ng-message="required">{{ \'ERRORS.REQUIRED\' | translate}}</div>' +
                    '</div>' +
                    '</md-input-container><md-input-container class="md-block" flex-gt-sm="">' +
                    '<label>{{ \'ACCOUNT_MANAGEMENT.PASSWORD\' | translate}}</label>' +
                    '<input type="password" name="password" ng-model="newAccount.password" required>' +
                    '<div class="hint" ng-show="showHints"></div>' +
                    '<div ng-messages="addAccountForm.password.$error">' +
                    '<div ng-message="required">{{ \'ERRORS.REQUIRED\' | translate}}</div></div>' +
                    '<span style="color: red; font-size: 14px">{{ error }}</span>' +
                    '<md-progress-linear md-mode="indeterminate" ng-show="tryingToLogin">' +
                    '</md-progress-linear></md-input-container>' +
                    '<section layout="row" layout-align="center center" layout-wrap>' +
                    '<md-button class="md-raised md-primary" ng-click="login()" ng-disabled="newAccount.username == undefined || newAccount.password == undefined || tryingToLogin">{{ \'ACCOUNT_MANAGEMENT.LOGIN\' | translate}}</md-button>' +
                    '<md-button class="md-raised" type="reset">{{ \'ACCOUNT_MANAGEMENT.RESET\' | translate}}</md-button></section></div>' +
                    '</form></md-input-container></md-dialog-site_content></form></md-dialog>',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                })
            };

            $scope.sendFeedback = function (ev) {

                $mdDialog.show({
                    controller: FeedbackController,
                    template: '<md-dialog aria-label="Send feedback" style="margin: 15px; max-width: 600px !important; max-height: 400px !important; width: 600px;">' +
                    '<md-toolbar><div class="md-toolbar-tools">' +
                    '<h2 style="margin-left: 22px">{{ \'FEEDBACK.TITLE\' | translate}}</h2>' +
                    '<span flex></span><md-button class="md-icon-button" ng-click="cancel()" style="margin: 0 !important;padding: 0 !important;">' +
                    '<md-icon aria-label="Close dialog" >close</md-icon></md-button></div></md-toolbar>' +
                    '<md-dialog-site_content><div class="md-dialog-site_content"><md-input-container class="md-block" style="margin: 0 !important;">' +
                    '<form name="feedbackForm">' +
                    '<md-input-container class="md-block">' +
                    '<label>{{ \'FEEDBACK.CONTENT\' | translate}}</label>' +
                    '<textarea ng-model="feedback" name="feedback" rows="5" md-select-on-focus>I</textarea></md-input-container>' +
                    '<section layout="row" layout-align="center center" layout-wrap>' +
                    '<md-button class="md-raised md-primary" ng-click="sendFeed()">{{ \'SEND\' | translate}}</md-button>' +
                    '<md-button class="md-raised" ng-click="cancel()">{{ \'CANCEL\' | translate}}</md-button></section>' +
                    '</form></md-dialog>',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                })
            };

            function FeedbackController($scope, $mdDialog, $translate) {
                $scope.newAccount = {};
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.sendFeed = function () {
                    $http.post('/feedback/add/', {
                        feedback: $scope.feedback
                    }).then(function mySucces(response) {
                        if (response.status == '200' && response.data == "ok") {
                            $scope.showInfoToast = function (message) {
                                $mdToast.show({
                                    hideDelay: 3000,
                                    position: 'bottom right',
                                    template: '<md-toast><md-icon style="color: rgb(250,250,250);">info_outline</md-icon><span class="md-toast-text" flex>' + message + '</span></md-toast>'
                                });
                            };
                            $scope.showInfoToast('Feedback was sent')
                            $scope.cancel()
                        }
                        $scope.tryingToLogin = false
                    }, function myError(response) {
                        $scope.showReportToast = function () {
                            $scope.showReport = false;
                            $mdToast.show({
                                hideDelay: 0,
                                position: 'bottom right',
                                controller: 'ToastReportCtrl',
                                template: '<md-toast ng-hide="showReport"><md-icon style="color: rgb(250,250,250);">report</md-icon><span class="md-toast-text" flex>Something was wrong <a href="/report/" style="color: rgb(250,34,34);">report</a><span flex></span>  <md-button ng-click="closeToast()">Close</md-button></span></md-toast>'
                            });
                        };
                        $scope.showReportToast()
                    });
                };

            }

            /**
             * Supplies a function that will continue to operate until the
             * time is up.
             */
            function debounce(func, wait, context) {
                var timer;

                return function debounced() {
                    var context = $scope,
                        args = Array.prototype.slice.call(arguments);
                    $timeout.cancel(timer);
                    timer = $timeout(function () {
                        timer = undefined;
                        func.apply(context, args);
                    }, wait || 10);
                };
            }

            /**
             * Build handler to open/close a SideNav; when animation finishes
             * report completion in console
             */
            function buildDelayedToggler(navID) {
                return debounce(function () {
                    $mdSidenav('left').toggle()
                }, 200);
            }

            function hideMenu() {
                $mdSidenav('left').close()
            }

            function DialogController($scope, $mdDialog, $translate) {
                $scope.newAccount = {};
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.login = function () {
                    $scope.error = "";

                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function () {
                        if (this.readyState == 4 && this.status == 200) {
                            resp = JSON.parse(xhttp.responseText)
                            $scope.ip = resp.ip
                        }
                    };
                    xhttp.open("GET", "//freegeoip.net/json/?callback=", false);
                    xhttp.send();

                    $scope.tryingToLogin = true;
                    $http.post('/account/add/', {
                        username: $scope.newAccount.username,
                        password: $scope.newAccount.password,
                        ip: $scope.ip
                    }).then(function mySucces(response) {
                            if (response.status == '200') {
                                if (JSON.stringify(response.data) == "\"902\"") {
                                    window.location = window.location
                                } else if (JSON.stringify(response.data) == "\"903\"") {
                                    $translate(['TOASTS.UNABLE_TO_LOGIN']).then(function (translations) {
                                        $scope.error = translations.UNABLE_TO_LOGIN
                                    });
                                }
                                else if (JSON.stringify(response.data) == "\"901\"") {
                                    $translate(['ACCOUNT_IN_USE']).then(function (translations) {
                                        $scope.error = translations.ACCOUNT_IN_USE
                                    });
                                }
                            }
                            $scope.tryingToLogin = false
                        }
                        ,
                        function myError(response) {
                            $rootScope.showReportToast()
                            $scope.tryingToLogin = false
                        }
                    );
                };

            }

            $scope.getSelectedText = function () {
                if ($scope.selectedMode !== undefined) {
                    return $scope.selectedMode;
                } else {
                    return "Please select a mode";
                }
            };

            $scope.showToast = function (toastMessage) {
                $mdToast.show({
                    hideDelay: 3000,
                    position: 'bottom right',
                    // controller: 'ToastCtrl',
                    template: '<md-toast>' +
                    '<span class="md-toast-text" flex>' + toastMessage + '</span>' +
                    '</md-toast>'
                });
            };

            $scope.submitDashboard = function () {
                $scope.checkActivity(false);


                if ($scope.dashboard.$valid) {
                    $scope.isDashboardDisabled = true;

                    $rootScope.showProgressToast();

                    var configs = {};

                    configs.follow_per_day = parseInt($scope.dashboard.followsPerHour.$viewValue) * 24;
                    configs.unfollow_per_day = parseInt($scope.dashboard.unfollowsPerHour.$viewValue) * 24;
                    configs.follow_time = parseInt($scope.dashboard.followTime.$viewValue) * 60;
                    configs.mode = parseInt($scope.dashboard.mode.$viewValue);

                    configs.like_per_day = parseInt($scope.dashboard.likesPerHour.$viewValue) * 24;
                    configs.max_like_for_one_tag = parseInt($scope.dashboard.likesPerTag.$viewValue);
                    configs.media_min_like = parseInt($scope.dashboard.minMediaLike.$viewValue);
                    configs.media_max_like = parseInt($scope.dashboard.maxMediaLike.$viewValue);

                    configs.comments_per_day = parseInt($scope.dashboard.commentsPerHour.$viewValue) * 24;
                    configs.comment_list = $scope.comments;

                    configs.tag_list = $scope.tags;
                    configs.tag_blacklist = $scope.tagBlackList;

                    configs.user_blacklist = $scope.usernameBlacklist;

                    // alert(JSON.stringify(configs) + "\n\n" + JSON.stringify(configs))

                    $http.post('/activity/start/', {configs: configs}).then(function (response) {
                        if (response.data == 'started') {
                            $rootScope.hideProgressToast()
                            $scope.showInfoToast('{{ \'TOASTS.STARTING_ACTIVITY\' | translate }}');
                            $scope.isDashboardDisabled = true;

                            $scope.startButtonDisabled = true
                            $scope.stopButtonDisabled = false
                        } else if (response.status == 500) {
                            $rootScope.hideProgressToast()
                            $scope.showReportToast();
                        }
                    })
                }
            };

            $scope.stopActivity = function () {
                $http.get('/activity/stop/').then(function (response) {
                    if (response.data = 'stopped') {
                        $scope.showInfoToast('{{ \'TOASTS.STOPPING_ACTIVITY\' | translate }}');
                        $scope.isDashboardDisabled = false;

                        $scope.startButtonDisabled = false
                        $scope.stopButtonDisabled = true

                    } else {
                        $scope.showReportToast()
                    }
                })
            };

            $scope.stopNoDisablingActivity = function () {
                $http.get('/activity/stop/').then(function (response) {
                    if (response.data == 'stopped') {
                        if (!$scope.isFailedToLogin) {
                            $scope.showInfoToast('{{ \'TOASTS.ACTIVITY_WAS_STOPPED\' | translate }}')
                        }
                    } else if (response.data == 'none') {

                    } else {
                        $scope.showReportToast()
                    }
                })
            };

            $scope.checkActivity = function (replaceValue) {

                replaceValue == undefined ? replaceValue = true : replaceValue = replaceValue

                $http.get('/activity/check/').then(function (response) {
                    if (response.status == 200 && response.data == 'none') {


                    } else if (response.status == 200 && response.data == 'expired') {

                        $scope.accountExpired = true;
                        $scope.isDashboardDisabled = true;

                        setContentStyle('none', 'block', 'none', true)
                        // open dialog that response payment
                    } else if (response.status == 200) {

                        if (replaceValue) {

                            $scope.template.followsPerHour = parseInt(response.data.follow_per_day) / 24;
                            $scope.template.unfollowsPerHour = parseInt(response.data.unfollow_per_day) / 24;
                            $scope.template.followTime = parseInt(response.data.follow_time) / 60;
                            $scope.template.mode = parseInt(response.data.mode);

                            $scope.template.likesPerHour = parseInt(response.data.like_per_day) / 24;
                            $scope.template.likesPerTag = parseInt(response.data.max_like_for_one_tag);
                            $scope.template.minMediaLike = parseInt(response.data.media_min_like);
                            $scope.template.maxMediaLike = parseInt(response.data.media_max_like);

                            $scope.template.commentsPerHour = parseInt(response.data.comments_per_day) / 24;

                            $scope.comments = response.data.comment_list;

                            $scope.tags = response.data.tag_list;
                            $scope.tagBlackList = response.data.tag_blacklist;

                            $scope.usernameBlacklist = response.data.user_blacklist;

                            // alert(response.data.status == 'finished')
                            // alert(response.data.status == 'started')
                            // $scope.isDashboardDisable d = true;
                            if ((response.data.status == 'finished') || response.data.status == undefined) {
                                $scope.activityRunningStatus = false
                            } else if (response.data.status == 'started') {
                                $scope.activityRunningStatus = true
                            } else {
                                $scope.showReportToast()
                            }
                        }
                    }

                    if (!$scope.accountExpired) {

                        $scope.checkStatuses()
                    }
                })
            };

            $scope.showMessageIfDashBoarDisabled = function () {
                if ($scope.isDashboardDisabled) {
                    if ($scope.isFailedToLogin) {
                        $scope.showFailedToLoginToast()
                    } else {
                        $scope.showInfoToast('{{\'TOASTS.STOP_ACTIVITY\' | translate}}')
                    }
                }
            };

            $scope.showInfoToast = function (message) {
                $mdToast.show({
                    hideDelay: 3000,
                    position: 'bottom right',
                    template: '<md-toast><md-icon style="color: rgb(250,250,250);">info_outline</md-icon><span class="md-toast-text" flex>' + message + '</span></md-toast>'
                });
            };

            $rootScope.showInfoToast = $scope.showInfoToast

            $scope.showReportToast = function () {
                $scope.showReport = false;
                $mdToast.show({
                    hideDelay: 0,
                    position: 'bottom right',
                    controller: 'ToastReportCtrl',
                    template: '<md-toast ng-hide="showReport"><md-icon style="color: rgb(250,250,250);">report</md-icon>' +
                    '<span class="md-toast-text" flex>{{\'TOASTS.ERROR.TITLE\' | translate}} <a ng-click="report()" style="color: rgb(250,34,34);">{{\'TOASTS.ERROR.REPORT\' | translate}}</a><span flex></span>  ' +
                    '<md-button ng-click="closeToast()">{{\'CLOSE\' | translate}}</md-button>' +
                    '</span>' +
                    '</md-toast>'
                });
            };

            $rootScope.showReportToast = $scope.showReportToast

            $scope.showWarningToast = function (message) {
                $mdToast.show({
                    hideDelay: 10000,
                    position: 'bottom right',
                    controller: 'ToastReportCtrl',
                    template: '<md-toast ng-hide="showReport">' +
                    '<md-icon style="color: rgb(250,250,250);">warning</md-icon>' +
                    '<span class="md-toast-text">' +
                    message +
                    '</span>' +
                    '<span flex></span><md-button ng-click="closeToast()">{{\'CLOSE\' | translate}}</md-button>' +
                    '</md-toast>'
                });
            };

            $rootScope.showWarningToast = $scope.showWarningToast;

            $scope.showProgressToast = function (message) {
                message == undefined ? message = '{{\'TOASTS.CONNECTING_TO_SERVER\' | translate}}' : message = message

                $mdToast.show({
                    hideDelay: 0,
                    position: 'bottom right',
                    controller: 'ToastReportCtrl',
                    template: '<md-toast>' +
                    '<md-progress-circular class="" style="stroke: rgb(250,250,250) !important;" md-diameter="20px"></md-progress-circular>' +
                    '<span class="md-toast-text" style="margin-left: 18px">' +
                    message +
                    '</span>' +
                    '</md-toast>'
                });

                $scope.hideProgressToast = function () {
                    $mdToast.hide()
                }

                $rootScope.hideProgressToast = $scope.hideProgressToast
            };

            $rootScope.showProgressToast = $scope.showProgressToast;

            $scope.showFailedToLoginToast = function (message) {
                message == undefined ? message = '{{\'TOASTS.UNABLE_TO_LOGIN\' | translate}}' : message = message

                $mdToast.show({
                    hideDelay: 10000,
                    position: 'bottom right',
                    controller: 'ToastReportCtrl',
                    template: '<md-toast ng-hide="showReport">' +
                    '<md-icon style="color: rgb(250,250,250);">warning</md-icon>' +
                    '<span class="md-toast-text" style="box-sizing: initial !important; -webkit-box-flex: initial; -webkit-flex: initial; min-width: initial;">' +
                    message +
                    '</span>' +
                    '<span flex></span><md-button ng-click="checkStatuses()">{{\'RETRY\' | translate}}</md-button><md-button ng-click="showSettings()">Settings</md-button>' +
                    '</md-toast>'
                });
            };

            $rootScope.showFailedToLoginToast = $scope.showFailedToLoginToast;

            $scope.showDashboard = function () {

                if (!$scope.isLoading) {

                    if ($scope.accountExpired) {
                        document.getElementById('site_content-dashboard-expired').style.display = "block";
                    } else if ($scope.isDashBoardEmpty) {
                        document.getElementById('content-dashboard-empty').style.display = "block";
                    } else {
                        document.getElementById('content-dashboard').style.display = "block";
                    }
                    document.getElementById('content-settings').style.display = "none";
                    hideMenu()
                }
            };

            $scope.showSettings = function () {

                if (!$scope.isLoading) {
                    document.getElementById('content-settings').style.display = "block";
                    document.getElementById('content-dashboard').style.display = "none";
                    document.getElementById('content-dashboard-empty').style.display = "none";
                    document.getElementById('site_content-dashboard-expired').style.display = "none";

                    $scope.checkInstagramAccounts();

                    $http.post('/accounts/info/', {
                        account_list: $scope.instagramAccounts
                    }).then(function mySuccess(response) {
                        if (response.status == 200) {
                            $scope.accountsInfo = JSON.parse(response.data)
                        }
                        hideMenu()
                    }, function myError(response) {
                        showReportToast();
                    });
                }
            };

            $rootScope.showSettings = $scope.showSettings

            $scope.accountPasswordChangeDialog = function (ev) {

                $mdDialog.show({
                    controller: 'accountPasswordController',
                    template: '<md-dialog aria-label="Change password" style="margin: 15px; max-width: 400px !important; width: 400px; max-height: 87% !important;">' +
                    '<md-toolbar><div class="md-toolbar-tools"><h2 style="margin-left: 22px">{{ \'ACCOUNT_MANAGEMENT.CHANGE_ACCOUNT_PASSWORD\' | translate}}</h2><span flex></span><md-button class="md-icon-button" ng-click="cancel()" style="margin: 0 !important;padding: 0 !important;"><md-icon aria-label="Close dialog" >close</md-icon></md-button></div></md-toolbar>' +
                    '<md-dialog-site_content>' +
                    '<div class="md-dialog-site_content" style="padding-bottom: 0px !important">' +
                    '<md-input-container class="md-block" style="margin: 0 !important;">' +
                    '<form name="passwordForm" >' +
                    '<md-input-container class="md-block">' +
                    '<label>{{ \'ACCOUNT_MANAGEMENT.CURRENT_PASSWORD\' | translate}}</label>' +
                    '<input required  ng-change="validatePasswords()" type="password" ng-model="passwords.oldPassword" name="oldPassword" >' +
                    '<div class="hint"></div><div ng-messages="passwordForm.oldPassword.$error"><div ng-message="required"></div></div>' +
                    '</md-input-container>' +
                    '<md-input-container class="md-block">' +
                    '<label>{{ \'ACCOUNT_MANAGEMENT.NEW_PASSWORD\' | translate}}</label>' +
                    '<input type="password" ng-model="passwords.newPassword" name="newPassword" ' +
                    ' minLength="8" required ng-pattern="/^(?=.*\\d)(?=.*[a-zA-Z])(?=.*[A-Z]).{1,}$/" ng-change="validatePasswords()"  >' +
                    '<div class="hint"></div><div ng-messages="passwordForm.newPassword.$error">' +
                    '<div ng-message="minlength">{{ \'VALIDATION.PASSWORD.LENGTH\' | translate}}</div>' +
                    '<div ng-message="pattern">{{ \'VALIDATION.PASSWORD.PATTERN\' | translate}}</div>' +
                    '<div ng-message="required"></div></div>' +
                    '</md-input-container>' +
                    '<md-input-container class="md-block">' +
                    '<label>{{ \'ACCOUNT_MANAGEMENT.REPEAT_NEW_PASSWORD\' | translate}}</label>' +
                    '<input required type="password" ng-change="validatePasswords()" ng-model="passwords.repeatPassword" name="repeatPassword" ' +
                    '<div class="hint"></div><div ng-messages="passwordForm.repeatPassword.$error"><div ng-message="required"></div></div>' +
                    '</md-input-container>' +
                    '<section layout="row" layout-align="center center" layout-wrap style="padding-bottom: 24px !important">' +
                    '<md-button class="md-raised md-primary" type="submit" ng-disabled="!isFormValid" layout="column" ng-click="submit()">' +
                    '<span>{{ \'SUBMIT\' | translate}}</span></md-button><md-button class="md-raised" ng-click="cancel()"  ng-init="validatePasswords()">{{ \'CANCEL\' | translate}}</md-button></section>' +
                    '</form></md-dialog>',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true
                })
            };


            $scope.instagramPasswordChangeDialog = function (ev, username) {
                $rootScope.instagramAccountChangeUsername = username;
                $mdDialog.show({
                    controller: instagramPasswordController,
                    template: '<md-dialog aria-label="Change password" style="margin: 15px; max-width: 400px !important; width: 400px;" ng-init="init()">' +
                    '<md-toolbar><div class="md-toolbar-tools"><h2 style="margin-left: 22px">{{ \'SETTINGS.CHANGE_PASSWORD\' | translate}}</h2><span flex></span><md-button class="md-icon-button" ng-click="cancel()" style="margin: 0 !important;padding: 0 !important;"><md-icon aria-label="Close dialog" >close</md-icon></md-button></div></md-toolbar>' +
                    '<md-dialog-site_content>' +
                    '<div class="md-dialog-site_content" style="padding-bottom: 0 !important">' +
                    '<md-input-container class="md-block" style="margin: 0 !important;">' +
                    '<form name="loginForm" ng-submit="submit()">' +
                    '<md-input-container class="md-block">' +
                    '<label>{{ \'ACCOUNT_MANAGEMENT.USERNAME\' | translate}}</label>' +
                    '<input required  name="username" ng-readonly="true" ng-model="username" >' +
                    '</md-input-container>' +
                    '<md-input-container class="md-block">' +
                    '<label>{{ \'ACCOUNT_MANAGEMENT.PASSWORD\' | translate}}</label>' +
                    '<input type="password" ng-model="instagram.password" name="password" required >' +
                    '<div class="hint"></div>' +
                    '<div ng-messages="instagram.newPassword.$error">' +
                    '<div ng-message="required"></div>' +
                    '</div>' +
                    '</md-input-container>' +
                    '<section layout="column" layout-align="center center" style="padding-bottom: 24px !important">' +
                    '<section layout="row" layout-align="center center" layout-wrap >' +
                    '<md-button class="md-raised md-primary" type="submit" layout="column" >' +
                    '<span>{{ \'SUBMIT\' | translate}}</span></md-button>' +
                    '<md-button class="md-raised" ng-click="cancel()" ">{{ \'CANCEL\' | translate}}</md-button>' +
                    '</section>' +
                    // '<a href ng-click="confirmDelete()">{{ \'ACCOUNT_MANAGEMENT.DELETE\' | translate}}</a>' +
                    '</section>' +
                    '</form></md-dialog>',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true
                })
            };

            function instagramPasswordController($scope, $rootScope, $mdDialog, $translate) {
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.init = function () {
                    $scope.username = $rootScope.instagramAccountChangeUsername
                };

                $scope.submit = function () {
                    // username = $scope.loginForm.username.$modelValue;
                    username = $scope.username;
                    password = $scope.loginForm.password.$modelValue;

                    $rootScope.showProgressToast();

                    $http.post('/account/password/set', {
                        username: username,
                        password: password
                    }).then(function mySucces(response) {
                        if (response.status == 200) {
                            if (response.data == 'modified') {
                                $rootScope.showInfoToast('Password was changed');
                                cancel()
                            } else if (response.data == 'login error') {
                                $rootScope.showInfoToast("{{ 'SETTINGS.ERRORS.UNABLE_TO_CHANGE_PASSWORD' | translate}}")
                            } else if (response.data == 'passwords same') {
                                $rootScope.showInfoToast('Passwords is same');
                                $rootScope.showInfoToast("{{ 'SETTINGS.ERRORS.PASSWORD_IS_SAME' | translate}}");
                            }
                        } else {
                            $rootScope.showReportToast()
                        }
                        $scope.hideProgressToast
                    });
                }
            }

            //ONLOAD
            $scope.checkStatuses = function () {
                $rootScope.showProgressToast('{{ \'TOASTS.TRYING_LOGIN_TO_INSTAGRAM\' | translate}}');
                $scope.startButtonDisabled = true;
                $scope.stopButtonDisabled = true;

                $http({
                    method: 'GET',
                    url: '/account/check/'
                }).then(function successResponse(response) {
                    $rootScope.hideProgressToast();

                    if (response.data == 'expired') {
                        setContentStyle('none', 'block', 'none')
                        $scope.stopNoDisablingActivity();
                        $scope.accountExpired = true;
                        $scope.isDashboardDisabled = true;
                    }

                    if (response.data == 'none') {
                        // TODO show prices instead dashboard
                        document.getElementById('content-dashboard').style.display = 'none';
                        document.getElementById('site_content-dashboard-expired').style.display = 'none';
                        document.getElementById('content-dashboard-empty').style.display = 'block';
                        $scope.isDashBoardEmpty = true;
                        return
                    }


                    response.data = JSON.parse(response.data);

                    if (!response.data.expireStatus) {
                        setContentStyle('none', 'block', 'none')
                        $scope.stopNoDisablingActivity();
                        $scope.accountExpired = true;
                        $scope.isDashboardDisabled = true;
                    } else if (!response.data.loginStatus) {
                        setContentStyle('block', 'none', 'none')
                        $scope.stopNoDisablingActivity();
                        $scope.isDashboardDisabled = true;
                        $scope.isFailedToLogin = true
                        $rootScope.showFailedToLoginToast()
                    } else if (response.data.runningActivities) {
                        setContentStyle('block', 'none', 'none')
                        $scope.isDashboardDisabled = true;
                        $scope.startButtonDisabled = true;
                        $scope.stopButtonDisabled = false;
                    } else if (!response.data.runningActivities) {
                        setContentStyle('block', 'none', 'none')
                        $scope.isDashboardDisabled = false;
                        $scope.startButtonDisabled = false;
                        $scope.stopButtonDisabled = true;
                    }
//Dashboard enabled, even if account failed to login/expired
                }, function errorResponse(response) {
                    $rootScope.showReportToast;
                    // $scope.isDashboardDisabled = false
                })
            };

            $rootScope.checkStatuses = $scope.checkStatuses


            $scope.setPrices = function (lang) {
                if ($scope.ipFrom == 'ua') {
                    $scope.prices = [
                        'UAH', ['189₴', '249₴', '699₴'], [189, 249, 699]
                    ];
                } else if (lang == 'en') {
                    $scope.prices = [
                        'USD', ['$ 6.99', '$ 9.99', '$ 26.99'], [6.99, 9.99, 26.99]
                    ];
                } else if (lang == 'ru') {
                    $scope.prices = [
                        'RUB', ['389₽', '559₽', '1499₽'], [389, 559, 1499]
                    ];
                }
            };

            $scope.setIp = function (l) {
                $http({
                    method: 'GET',
                    url: 'https://freegeoip.net/json/?callback='
                }).then(function successCallback(resp) {
                    resp = resp.data
                    $scope.ip = resp.ip
                    $scope.ipFrom = resp.country_code.toLowerCase()
                    $scope.setPrices(l)
                }, function errorCallback(resp) {
                });
            };

            $scope.requestToAcquiring = function (username, months) {
                var expr = new Date();
                expr.setDate(expr.getDate() + 3);
                $cookies.put('username_acq', username, {
                    expires: expr
                });
                $cookies.put('months_acq', months, {
                    expires: expr
                });
                window.location.href = '/acquiring/checkout/'
            };

            $scope.isLoading = true;
            function setContentStyle(dashboard, expired, empty, isDashBoardExpired) {
                document.getElementById('content-dashboard').style.display = dashboard;
                document.getElementById('content-dashboard-expired').style.display = expired;
                document.getElementById('content-dashboard-empty').style.display = empty;
                $scope.isDashBoardEmpty = isDashBoardExpired;

                document.getElementById('loader').style.display = 'none';
                $scope.isLoading = false;
            }
        }
    )
    .controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
        $scope.close = function () {
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav('left').close()
                .then(function () {
                });
        };
    })
    .controller('RightCtrl', function ($scope, $timeout, $mdSidenav, $log) {
        $scope.close = function () {
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav('right').close()
                .then(function () {
                });
        };
    })
    .controller('accountPasswordController', function ($scope, $rootScope, $mdDialog, $http) {
        $scope.cancel = function () {
            $mdDialog.cancel();
        };


        $scope.validatePasswords = function () {
            var newPassword = $scope.passwordForm.newPassword.$modelValue;
            var repeatPassword = $scope.passwordForm.repeatPassword.$modelValue;

            $scope.isFormValid = $scope.passwordForm.$valid && (newPassword == repeatPassword)
        };

        $scope.submit = function () {

            $scope.isFormValid = false;

            var oldPassword = $scope.passwordForm.oldPassword.$modelValue;
            var newPassword = $scope.passwordForm.newPassword.$modelValue;
            var repeatPassword = $scope.passwordForm.repeatPassword.$modelValue;


            $http.post('/password/change/', {
                old_password: oldPassword,
                new_password: newPassword
            }).then(function mySuccess(response) {

                if (response.status == 200) {
                    if (response.data == '901') {
                        $rootScope.showWarningToast("{{ 'ACCOUNT_MANAGEMENT.ERRORS.PASSWORDS_MATCH' | translate}}")
                        for (passw in [oldPassword, newPassword, repeatPassword]) {
                            passw = ''
                        }
                    } else if (response.data == '902') {

                        $rootScope.showInfoToast("{{ 'ACCOUNT_MANAGEMENT.ERRORS.PASSWORDS_WAS_CHANGED' | translate}}");

                        setTimeout(function () {
                            location.reload()
                        }, 3000)

                    } else if (response.data == '903') {

                        $rootScope.showWarningToast("{{ 'ACCOUNT_MANAGEMENT.ERRORS.PASSWORDS_DONT_MATCH' | translate}}")
                        $scope.passwordForm.oldPassword.$modelValue = ''
                        $scope.passwordForm.oldPassword.$viewValue = ''
                        $scope.passwordForm.newPassword.$modelValue = ''
                        $scope.passwordForm.newPassword.$viewValue = ''
                        $scope.passwordForm.repeatPassword.$modelValue = ''
                        $scope.passwordForm.repeatPassword.$viewValue = ''
                    }
                }
            }, function myError(response) {
                showReportToast();
            });
        }
    })
    .controller('ToastReportCtrl', function ($scope, $mdToast, $mdDialog) {

        $scope.closeToast = function () {
            if (isDlgOpen) return;

            $mdToast
                .hide()
                .then(function () {
                    isDlgOpen = false;
                });
        };

        $scope.report = function (ev) {

            $mdDialog.show({
                controller: ReportController,
                template: '<md-dialog aria-label="Send feedback" style="margin: 15px; max-width: 600px !important; max-height: 400px !important; width: 600px;">' +
                '<md-toolbar><div class="md-toolbar-tools"><h2 style="margin-left: 22px">Send report</h2><span flex></span><md-button class="md-icon-button" ng-click="cancel()" style="margin: 0 !important;padding: 0 !important;"><md-icon aria-label="Close dialog" >close</md-icon></md-button></div></md-toolbar>' +
                '<md-dialog-site_content><div class="md-dialog-site_content"><md-input-container class="md-block" style="margin: 0 !important;">' +
                '<form name="reportForm">' +
                '<md-input-container class="md-block">' +
                '<label>Please, describe your problem. For screenshots use <a style="text-decoration: underline;">http://imgur.com/</a></label>' +
                '<textarea ng-model="report" name="report" rows="5" md-select-on-focus>I</textarea></md-input-container>' +
                '<section layout="row" layout-align="center center" layout-wrap><md-button class="md-raised md-primary" ng-click="sendReport()">Send</md-button><md-button class="md-raised" ng-click="cancel()">Cancel</md-button></section>' +
                '</form></md-dialog>',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            })
        };

        function ReportController($scope, $mdDialog, $http, $mdToast) {

            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.sendReport = function () {
                $http.post('/report/add/', {
                    report: $scope.reportForm.report.$viewValue
                }).then(function mySucces(response) {
                    if (response.status == '200' && response.data == "ok") {
                        $scope.showInfoToast = function (message) {
                            $mdToast.show({
                                hideDelay: 3000,
                                position: 'bottom right',
                                template: '<md-toast><md-icon style="color: rgb(250,250,250);">info_outline</md-icon><span class="md-toast-text" flex>' + message + '</span></md-toast>'
                            });
                        };
                        $scope.showInfoToast('Report was sent');
                        $scope.cancel()
                    }
                    $scope.tryingToLogin = false
                }, function myError(response) {
                    $scope.showReportToast = function () {
                        $scope.showReport = false;
                        $mdToast.show({
                            hideDelay: 0,
                            position: 'bottom right',
                            controller: 'ToastReportCtrl',
                            template: '<md-toast ng-hide="showReport"><md-icon style="color: rgb(250,250,250);">report</md-icon><span class="md-toast-text" flex>Something was wrong <a href="/report/" style="color: rgb(250,34,34);">report</a><span flex></span>  <md-button ng-click="closeToast()">Close</md-button></span></md-toast>'
                        });
                    };
                    $rootScope.showReportToast()
                });
            };
        }


    });


app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

var translationsEN = {
    SUBMIT: 'Submit',
    SEND: 'Send',
    CANCEL: 'Cancel',
    RETRY: 'Retry',
    CLOSE: 'Close',
    ACCOUNT_IN_USE: 'Account already in use',
    MENU: {
        DASHBOARD: 'Dashboard',
        PLANS: 'Plans',
        SETTINGS: 'Settings',
        HELP: 'Help',
        SENDFEEDBACK: 'Send feedback',
        SIGNOUT: 'Sign out',
        TERMSOFSERVICE: 'Terms of service',
        PRIVACYPOLICY: 'Privacy policy',
        FAQ: 'FAQ',
        ABOUT: 'About'
    },
    FIRST_TUTORIAL: {
        DASHBOARD_TITLE: 'You should to add account, before start work'
    },
    ACCOUNT_MANAGEMENT: {
        ADD_ACCOUNT: 'Add account',
        USERNAME: 'Username',
        PASSWORD: 'Password',
        LOGIN: 'Log in',
        RESET: 'Reset',
        DELETE: 'Delete',
        DELETE_MESSAGE: 'Are you sure? All data include ' +
        'subscription will be permanently removed. ' +
        'For decline press cancel.',
        INVALID_CREDENTIALS: 'Check credentials',
        CHANGE_ACCOUNT_PASSWORD: 'Change account password',
        CURRENT_PASSWORD: 'Current password',
        NEW_PASSWORD: 'New password',
        REPEAT_NEW_PASSWORD: 'Repeat new password',
        ERRORS: {
            PASSWORDS_DONT_MATCH: 'Password don\'t match',
            PASSWORDS_MATCH: 'Old and new passwords match',
            PASSWORD_WAS_CHANGED: 'Password was changed'
        }
    },
    DASHBOARD: {
        BUTTONS: {
            START: 'Start',
            STOP: 'Stop',
            TIPS: 'tips'
        },
        ERRORS: {
            BAN: 'Ban is possible',
            NUMBER: 'Numbers only',
            VALUE: 'Invalid value'
        },
        FOLLOWS: {
            TITLE: 'Follows',
            FOLLOWS_PER_HOUR: {
                TITLE: 'Follows per hour',
                TOOLTIP: 'How many users to follow in hour.'
            },
            UNFOLLOWS_PER_HOUR: {
                TITLE: 'Unfollows per hour',
                TOOLTIP: 'How many users to unfollow in hour.'
            },
            FOLLOW_TIME: {
                TITLE: 'Follow time',
                TOOLTIP: 'Minutes before unfollow user. For never use 0.'
            },
            MODES: {
                TITLE: 'Mode',
                TOOLTIP: 'Choose mode',
                MODES: ['Original', 'Modified', 'Unfollow who don\'t follow back', 'Follow base on recent feed.',
                    'Unfollow based on recent feed', 'Unfollow everybody'],
                MODES_TOOLTIPS: ['Original mode.', 'Modified mode. More efficient than original.',
                    'Unfollow who don\'t follow back mode.', 'Follow base on recent feed mode.',
                    'Unfollow based on recent feed mode.', 'Unfollow everybody. Don\'t use this mode rarely']
            }
        },
        LIKES: {
            TITLE: 'Likes',
            LIKES_PER_HOUR: {
                TITLE: 'Likes per hour',
                TOOLTIP: 'How many users to follow in hour.'
            },
            LIKES_PER_TAG: {
                TITLE: 'Likes per tag',
                TOOLTIP: 'How many posts will be liked in tag.'
            },
            POST_MIN_LIKES: {
                TITLE: 'Post min likes',
                TOOLTIP: 'Don\'t like, if post have less than N likes.'
            },
            POST_MAX_LIKES: {
                TITLE: 'Post max likes',
                TOOLTIP: 'Don\'t like, if post have more than N likes.'
            }
        },
        COMMENTS: {
            TITLE: 'Comments',
            ADD_COMMENT: 'Add comment',
            ADD_MORE_COMMENTS: '+ Add comment',
            COMMENTS_PER_HOUR: {
                TITLE: 'Comments per hour',
                TOOLTIP: 'How many posts will be comment in hour.'
            }
        },
        TAGS: {
            TITLE: 'Tags',
            ADD_TAG: 'Add tag',
            ADD_MORE_TAGS: '+ Add tag',
            ADD_IGNORE_TAG: 'Ignore tag',
            ADD_MORE_IGNORE_TAGS: '- Ignore tag'
        },
        USERBLACKLIST: {
            TITLE: 'User blacklist',
            ADD_IGNORE_USER: 'Ignore user',
            ADD_MORE_IGNORE_USER: '- Ignore user'
        }
    },
    TOASTS: {
        LANGUAGE: {
            CHANGE_SUCCESSFUL: 'Language was changed'
        },
        ERROR: {
            TITLE: 'Something was wrong...',
            REPORT: 'Report'
        },
        UNABLE_TO_LOGIN: 'Unable to login. Check credentials.',
        CONNECTING_TO_SERVER: 'Connecting to server...',
        TRYING_LOGIN_TO_INSTAGRAM: 'Trying login to instagram...',
        STARTING_ACTIVITY: 'Staring a new activity',
        STOPPING_ACTIVITY: 'Stopping a new activity',
        STOP_ACTIVITY: 'You need to stop activity, before make changes',
        ACTIVITY_WAS_STOPPED: 'Activity was stopped'
    },
    PLANS: {
        TITLES: {
            BASE: "Base",
            PRO: "Pro",
            ADVANCED: "Advanced"
        },
        FUNCTIONS: {
            UNLIMITED_USAGE: "Unlimited usage",
            UNLIMITED_FOLLOWS: "Unlimited follows",
            UNLIMITED_LIKES: "Unlimited likes",
            UNLIMITED_COMMENTS: "Unlimited comments",
            PRIORITY_SUPPORT: "Priority support",
            TWO_MONTH_FREE: "Two month FREE",
            BETA_FEATURES: "Beta features",
            BASE_SUPPORT: "Base support",
            UPDATE: "Get it!",
            PLAN_EXPIRED: "Your plan was expired. Choose one of plans.",
        },
        MONTHS: {
            1: 'month',
            2: 'months',
            6: 'months'
        }
    },
    SETTINGS: {
        EMAIL: 'Email',
        CHANGE_PASSWORD: 'Change password',
        USERNAME: 'Username',
        EXPIRED_DATE: 'Expired date',
        RENEW_SUBSCRIPTION: 'Renew subscription',
        PASSWORD_WAS_CHANGED: 'Password was changed',
        ERRORS: {
            UNABLE_TO_CHANGE_PASSWORD: 'Unable to change password. Check your credentials',
            PASSWORD_IS_SAME: 'Password is same'
        }
    },
    FEEDBACK: {
        TITLE: 'Send feedback',
        CONTENT: 'Please, describe your feelings, or report a bug.'
    },
    VALIDATION: {
        PASSWORD: {
            LENGTH: 'Password should contain at least 8 characters',
            PATTERN: 'Password should contain at least one digit, one upper case and one lower case character',
        }
    }
};

var translationsRU = {
    SUBMIT: 'Подтвердить',
    SEND: 'Отправить',
    CANCEL: 'Отменить',
    RETRY: 'Повторить',
    CLOSE: 'Закрыть',
    ACCOUNT_IN_USE: 'Аккаунт с таким логином, уже зарегистрирован',
    MENU: {
        DASHBOARD: 'Активность',
        PLANS: 'Тарифы',
        SETTINGS: 'Настройки',
        HELP: 'Помощь',
        SENDFEEDBACK: 'Отправить отзыв',
        SIGNOUT: 'Выход',
        TERMSOFSERVICE: 'Условия',
        PRIVACYPOLICY: 'Приватность',
        FAQ: 'FAQ',
        ABOUT: 'О нас'
    },
    FIRST_TUTORIAL: {
        DASHBOARD_TITLE: 'Добавьте аккаунт, для начала работы.'
    },
    ACCOUNT_MANAGEMENT: {
        ADD_ACCOUNT: 'Добавить аккаунт',
        USERNAME: 'Имя пользователя',
        PASSWORD: 'Пароль',
        LOGIN: 'Войти',
        RESET: 'Очистить',
        DELETE: 'Удалить',
        DELETE_MESSAGE: 'Вы уверенны? Все данные включая подписку, ' +
        'будут удаленны навсегда. Для отказа нажмите "Отмена".',
        INVALID_CREDENTIALS: 'Неверный пароль или имя пользователя',
        CHANGE_ACCOUNT_PASSWORD: 'Смена пароля',
        CURRENT_PASSWORD: 'Текущий пароль',
        NEW_PASSWORD: 'Новый пароль',
        REPEAT_NEW_PASSWORD: 'Повторите пароль',
        ERRORS: {
            PASSWORDS_DONT_MATCH: 'Пароли не совпадают',
            PASSWORDS_MATCH: 'Старый и новый пароль совпадат',
            PASSWORD_WAS_CHANGED: 'Пароль изменён'
        }
    },
    DASHBOARD: {
        BUTTONS: {
            START: 'Старт',
            STOP: 'Стоп',
            TIPS: 'Подсказки',
            REQUIRED: 'Обязательное'
        },
        ERRORS: {
            BAN: 'Возможна блокировка аккаунта',
            NUMBER: 'Только цифры',
            VALUE: 'Некоректное значение'
        },
        FOLLOWS: {
            TITLE: 'Подписки',
            FOLLOWS_PER_HOUR: {
                TITLE: 'Подписок/час',
                TOOLTIP: 'Подписатся на N пользователей в час.'
            },
            UNFOLLOWS_PER_HOUR: {
                TITLE: 'Отписок/час',
                TOOLTIP: 'Отписатся от N пользователей в час.'
            },
            FOLLOW_TIME: {
                TITLE: 'Время подписки',
                TOOLTIP: 'Отписка от пользователя через N минут.'
            },
            MODES: {
                TITLE: 'Режим',
                TOOLTIP: 'Выберите режим',
                MODES: ['Стандартный', 'Улучшеный', 'Отписатся от тех, кто не подписался.', 'Подписка на основе последних постов.', 'Отписка на основе последних постов', 'Отписатся от всех'],
                MODES_TOOLTIPS: ['Стандартный режим', 'Улучшеный режим. Более функциональный.',
                    'Отписатся от тех, кто не подписался.', 'Подписка на основе последних постов.',
                    'Отписка на основе последних постов', 'Отписатся от всех']
            }
        },
        LIKES: {
            TITLE: 'Лайки',
            LIKES_PER_HOUR: {
                TITLE: 'Лайков/час',
                TOOLTIP: 'Подписаться на N пользователей в час.'
            },
            LIKES_PER_TAG: {
                TITLE: 'Лайков/хэштег',
                TOOLTIP: 'Ставить N лайков на один хэштег.'
            },
            POST_MIN_LIKES: {
                TITLE: 'Минимум лайков',
                TOOLTIP: 'Игнорировать, если пост имеет менее N лайков.'
            },
            POST_MAX_LIKES: {
                TITLE: 'Максимум лайков',
                TOOLTIP: 'Игнорировать, если пост имеет более N лайков.'
            }
        },
        COMMENTS: {
            TITLE: 'Комментарии',
            ADD_COMMENT: 'Добавить комментарий',
            ADD_MORE_COMMENTS: '+ Добавить комментарий',
            COMMENTS_PER_HOUR: {
                TITLE: 'Комментариев/час',
                TOOLTIP: 'Прокомментировать N постов в час.'
            }
        },
        TAGS: {
            TITLE: 'Хэштеги',
            ADD_TAG: 'Добавить хэштег',
            ADD_MORE_TAGS: '+ Добавить хэштег',
            ADD_IGNORE_TAG: 'Игнорировать хэштег',
            ADD_MORE_IGNORE_TAGS: '- Игнорировать хэштег'
        },
        USERBLACKLIST: {
            TITLE: 'Чёрный список',
            ADD_IGNORE_USER: 'Игнорировать пользователя',
            ADD_MORE_IGNORE_USER: '- Игнорировать пользователя'
        }
    },
    TOASTS: {
        LANGUAGE: {
            CHANGE_SUCCESSFUL: 'Выбран русский язык'
        },
        ERROR: {
            TITLE: 'Что-то пошло не так...',
            REPORT: 'Сообщить'
        },
        UNABLE_TO_LOGIN: 'Невозможно войти в аккаунт. Невернный логин, или пароль',
        CONNECTING_TO_SERVER: 'Соединение с сервером...',
        TRYING_LOGIN_TO_INSTAGRAM: 'Соединение с инстаграмом...',
        STARTING_ACTIVITY: 'Запуск новой активности',
        STOPPING_ACTIVITY: 'Остановка активности',
        STOP_ACTIVITY: 'Сначала остановите активность',
        ACTIVITY_WAS_STOPPED: 'Активность остановлена'
    },
    PLANS: {
        TITLES: {
            BASE: "Базовый",
            PRO: "Стандартный",
            ADVANCED: "Расширеный"
        },
        FUNCTIONS: {
            UNLIMITED_USAGE: "Использование ∞",
            UNLIMITED_FOLLOWS: "Подписки ∞",
            UNLIMITED_LIKES: "Лайки ∞",
            UNLIMITED_COMMENTS: "Комментарии ∞",
            TWO_MONTH_FREE: "2 месяца бессплатно",
            PRIORITY_SUPPORT: "Приоритетная поддержка",
            BETA_FEATURES: "Beta функции",
            BASE_SUPPORT: "Базовая поддержка",
            UPDATE: "Получить",
            PLAN_EXPIRED: "Ваша подписка приостановлена. Выберите один из планов.",
        },
        MONTHS: {
            1: 'месяц',
            2: 'месяца',
            6: 'месяцев'
        }
    },
    SETTINGS: {
        EMAIL: 'Email',
        CHANGE_PASSWORD: 'Сменить пароль',
        USERNAME: 'Имя пользователя',
        EXPIRED_DATE: 'Подписка до',
        RENEW_SUBSCRIPTION: 'Продлить подписку',
        PASSWORD_WAS_CHANGED: 'Параль изменён',
        ERRORS: {
            UNABLE_TO_CHANGE_PASSWORD: 'Неверный пароль, или имя пользователя',
            PASSWORD_IS_SAME: 'Пароли одинаковые'
        }
    },
    FEEDBACK: {
        TITLE: 'Отправить отзыв',
        CONTENT: 'Пожалуйтса, опишите свой опыт, или сообщите о проблеме.'
    },
    VALIDATION: {
        PASSWORD: {
            LENGTH: 'Пароль должен быть не менее 8 символов',
            PATTERN: 'Пароль должен содержать одну, одну букву верхнего и одну букву нижнего регистра',
        }
    },
    PRICING: {
        SUBHEADER: 'Ваш пробный период истёк, выберите один из тарифов',
    }
};

app.config(['$translateProvider', function ($translateProvider) {
    $translateProvider.translations('en', translationsEN);
    $translateProvider.translations('ru', translationsRU);
    $translateProvider.useSanitizeValueStrategy('escape');
}]);
