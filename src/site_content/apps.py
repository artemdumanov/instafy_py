from django.apps import AppConfig


class ContentConfig(AppConfig):
    name = 'site_content'
