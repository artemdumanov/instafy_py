from django.shortcuts import render
from django.views import View
from django.views.generic import TemplateView

from models.models import InstagramAccount
from utils import logging

log = logging.getLogger()


# TODO make all pages as app
class LandingView(TemplateView):
    template_name = 'index.html'


class FaqView(TemplateView):
    template_name = 'faq.html'


class TermsView(TemplateView):
    template_name = 'terms.html'


class PrivacyView(TemplateView):
    template_name = 'privacy.html'


class PricingView(View):
    def get(self, request, *args, **kwargs):
        try:
            user = request.user
            if user.is_authenticated():
                instagrams = InstagramAccount.objects.filter(account_id_account=request.user.id_account)
                instagrams = [instagram.username for instagram in instagrams]
                log.debug('Render pricing page for {}, instagrams {}'
                          .format(user, instagrams))

                return render(request, template_name='pricing.html',
                              context={'instagrams': instagrams})
            else:
                log.debug('Render pricing page for {}, no instagrams'
                          .format(user))
                return render(request, template_name='pricing.html')
        except Exception as e:
            log.exception(e)
            raise
