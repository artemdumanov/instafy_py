var prices = [];
var selectedIndex = 0;
var selectMonths = document.getElementById("months");
var language = undefined;

selectMonths.onchange = function () {
    var index = selectMonths.selectedIndex;
    if (index === 0) {
        selectMonths.style.width = "80px"
    } else if (index === 1) {
        selectMonths.style.width = "108px";
    } else if (index === 2) {
        selectMonths.style.width = "117px"
    }
    selectedIndex = index;
    setPrices(language)
};

function setPrices(lang) {
    if (lang == 'ua') {
        prices = [
            'UAH', ['189₴', '249₴', '699₴'], [189, 249, 699]
        ];
    } else if (lang == 'en') {
        prices = [
            // 'USD', ['$ 6.99', '$ 9.99', '$ 26.99'], [6.99, 9.99, 26.99]
            'USD', ['$6.99', '$9.99', '$26.99'], [6.99, 9.99, 26.99]
        ];
    } else if (lang == 'ru') {
        prices = [
            'RUB', ['389₽', '559₽', '1499₽'], [389, 559, 1499]
        ];
    }
    var price = document.getElementById("paid-price");
    price.innerHTML = prices[1][selectedIndex]
}

function setIp() {
    // language = 'en';
    // setPrices('en')
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://freegeoip.net/json/?callback=');
    xhr.onload = function (e) {
        if (xhr.status != 200) {
            language = 'en';
            setPrices('en')
        } else {
            var data = xhr.responseText;
            data = JSON.parse(data);
            var lang = data.country_code.toLowerCase();
            language = lang;
            setPrices(lang)
        }
    };
    xhr.send(null);
}
setIp();
