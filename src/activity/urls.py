from django.conf.urls import url

from activity import views

urlpatterns =[
    url(r'start/$', views.start_activity, name="create_bot"),
    url(r'status/$', views.get_activity_status, name="activity_status"),
    url(r'check/$', views.check_activity, name="check_activity"),
    url(r'stop/$', views.stop_activity, name="stop_activity"),
]
