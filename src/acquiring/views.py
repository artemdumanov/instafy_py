import json

from django.http import HttpResponse, HttpResponseServerError
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from models.models import InstagramAccount
from utils import logging
from utils.crypto import QuoteEncryptr
from utils.proxy import ProxyManager

log = logging.getLogger()
manager = ProxyManager()


@csrf_exempt
def callback(request):
    try:
        # if request.method == 'POST':
        encoded_param = request.GET.get('hash')
        body_unicode = request.body.decode('utf-8')
        resp = json.loads(body_unicode)

        if 'response_status' in resp and \
                        'order_status' in resp and \
                        'tran_type' in resp and \
                        resp['response_status'] == 'success' and \
                        resp['order_status'] == 'approved' and \
                        resp['tran_type'] == 'purchase':
            decoded_data = QuoteEncryptr.decode(encoded_param)
            username, period = decoded_data.split(';')
            instagram = InstagramAccount.objects.get(username=username)
            # proxy = Proxy.objects.get(account=instagram)
            try:
                # print(username)
                # print(period)
                # print('Prolong...')
                manager.prolong(instagram, 'basic', period)
            except Exception as e:
                log.exception('Something goes wrong when try '
                              'to prolong proxy of {} for {} days'
                              .format(username, period))
                raise
            log.info('Successful prolong proxy of {} for {} days; '
                     'callback {}'.format(username, period, resp))
            return HttpResponse()
        else:
            log.info('Suspicious callback {}'.format(resp))
            log.warning('Suspicious callback {}'.format(resp))
            return HttpResponseServerError()
        # else:
        #     return HttpResponseServerError()
    except Exception as e:
        raise


pricing = {
    'ua': {
        'currency': 'UAH',
        'months': {
            1: 189,
            2: 249,
            6: 699
        }
    },
    'us': {
        'currency': 'USD',
        'months': {
            1: 6.99,
            2: 9.99,
            6: 26.99
        }
    },
    'ru': {
        'currency': 'RUB',
        'months': {
            1: 389,
            2: 559,
            6: 1499
        }
    }
}


# @login_required
# @method_decorator(login_required, name='dispatch')
class Checkout(View):
    def get(self, request):
        try:
            cookies = request.COOKIES
            self.username = request.GET.get('username')
            self.months = request.GET.get('months')
            self.currency = request.GET.get('currency')

            url = '/acquiring/checkout/'
            log.debug('Trying to acquire. Params {} for months code {}, '
                      'currency is {}'.format(self.username,
                                              self.months,
                                              self.currency))
            price = pricing['us']
            if self.currency is not None:
                request.session['currency'] = 'USD'
                price = pricing['us']
                for country in pricing:
                    if pricing[country]['currency'] == self.currency:
                        request.session['currency'] = self.currency
                        price = pricing[country]
            elif 'currency' in request.session is not None:
                # price = [pricing[country] for country in pricing
                #          if pricing[country]['currency'] == request.session['currency']]
                for country in pricing:
                    if pricing[country]['currency'] == request.session['currency']:
                        price = pricing[country]

            else:
                request.session['currency'] = 'USD'
                # price = [pricing[country] for country in pricing
                #          if pricing[country]['currency'] == request.session['currency']]
                for country in pricing:
                    if pricing[country]['currency'] == request.session['currency']:
                        price = pricing[country]

            try:
                if self.username is None or self.months is None:
                    if self.username is None:
                        self.username = str(cookies.get('username'))
                    else:
                        if self.months is None:
                            if cookies['months'] is not None:
                                self.months = int(cookies['months'])
                            else:
                                self.months = 2

                        resp = redirect('{}?months={}'.format(url, self.months))
                        resp.set_cookie('username', self.username)
                        resp.set_cookie('months', self.months)
                        return resp
                    if self.months is None:
                        self.months = int(cookies.get('months'))
                    else:
                        resp = redirect(url)
                        resp.set_cookie('username', self.username)
                        resp.set_cookie('months', self.months)
                        return resp
                else:
                    self.username = str(self.username)
                    self.months = int(self.months)
                    resp = redirect(url)
                    resp.set_cookie('username', self.username)
                    resp.set_cookie('months', self.months)
                    return resp
                log.debug('Acquire {} for {} months'
                          .format(self.username, self.months))

            except Exception as e:
                log.exception('Bad params on acquiring')
                raise
            try:
                InstagramAccount.objects.get(username=self.username)
            except InstagramAccount.DoesNotExist:
                log.exception('Tried to acquire not existing instagram account'
                              .format(self.username))
                raise

            self.months = int(self.months)
            if self.months not in (1, 2, 6):
                log.exception('Invalid param')
            period = self.months * 30

            amount = price['months'][self.months]
            callback_url_param_data = '{};{}'.format(self.username, period)
            callback_url_param = QuoteEncryptr.encode(callback_url_param_data)

            context = {
                'currency': price['currency'],
                'amount': amount,
                'callback_url_param': callback_url_param
            }

            request.session['amount'] = amount
            return render(request, "checkout.html", context=context)
        except Exception as e:
            log.exception(e)
            raise


class SuccessAcquiring(View):
    template_name = "success.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(SuccessAcquiring, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        post = request.POST
        # print('Post is {}'.format(json.dumps(post, sort_keys=True, indent=4)))
        return render(request, self.template_name)
