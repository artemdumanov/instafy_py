from django.db import models
from django.contrib.auth.base_user import BaseUserManager

isreq = ' is required'


class AccountManager(BaseUserManager):
    def create_user(self, email, password, registration_date, plan, expired_date, checked):
        user = self.model(
            email=self.normalize_email(email),
            plan=plan,
            expired_date=expired_date,
            registration_date=registration_date,
            checked=checked
        )

        user.set_password(password)
        user.save()
        return user
