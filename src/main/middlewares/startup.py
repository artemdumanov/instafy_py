from activity.vulture import Vulture, enable_daily_expired_accounts_check
from utils import logging
from utils.proxy import ProxyManager

log = logging.getLogger()


class StartupMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

        try:
            ProxyManager()
            Vulture()
            enable_daily_expired_accounts_check()
        except Exception as e:
            log.exception(e)
            raise

    def __call__(self, request):
        response = self.get_response(request)

        return response

