import json
from datetime import datetime, timedelta

from django import views
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.template import loader
from django.template.loader import get_template, render_to_string
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect, csrf_exempt

from main.settings import DATE_FORMAT
from models.models import Account
from utils import logging
from utils.proxy import ProxyManager

log = logging.getLogger()
prx = ProxyManager()


class SignUp(views.View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('/')

        return render(request, 'signup.html')

    @method_decorator(csrf_protect)
    def post(self, request, *args, **kwargs):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)

        email = body['email']
        password = body['password']

        account = Account()
        account.email = email
        account.set_password(password)
        account.account_count = 0
        account.is_active = 0
        account.language = 'ru'
        # account.language = request.COOKIES['language']

        today = datetime.today()
        start = today.strftime(DATE_FORMAT)
        end = (today + timedelta(days=3)).strftime(DATE_FORMAT)

        account.trial = '["{}", "{}"]'.format(start, end)
        account.trial_end_time = datetime.now().time()
        account.save()

        log.info('Signed up as ' + account.email)

        welcome_email = render_to_string(template_name='welcome-email.html',
                                         context={'email': 'artem.dymanov@gmail.com'})
        # TODO what about en version of emails?
        send_mail(
            subject='Добро пожаловать в комманду uGram.co',
            message=welcome_email,
            html_message=welcome_email,
            from_email='support@ugram.co',
            recipient_list=[email],
            # recipient_list=['support@ugram.co'],
            fail_silently=False,
        )

        return redirect('/app/')


class SignIn(views.View):
    def get(self, request, *args, **kwargs):

        if request.user.is_authenticated:
            return redirect('/app/')

        return render(request, 'signin.html')

    @method_decorator(csrf_protect)
    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            email = request.POST['email']
            password = request.POST['password']

            user = authenticate(username=email, password=password)

            if user is not None:
                user = authenticate(username=email, password=password)

                if user is not None:
                    login(request, user)
                    log.info('Signed in as ' + user.email)

                return redirect('/')
            else:
                kwargs = {
                    "Error": "Please check that you have entered email "
                             "and password correctly.",
                    "email": email
                }
                log.info('Fail to sign in as  ' + email)

                return render(request, 'signin.html', kwargs) if \
                    request.GET.get('next', None) is not None else \
                    redirect(request.GET.get('next'))


@csrf_exempt
def validate_username(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    try:
        email = body['email']
    except KeyError as e:
        log.exception(e)
        raise

    log.debug('Trying to check availability of email: {}'
              .format(email))

    data = {
        'is_taken': Account.objects.filter(email__iexact=email)
            .exists()
    }
    return JsonResponse(data)


@login_required
def sign_out(request):
    log.debug('Sign out: {}'.format(request.user))
    logout(request)

    return redirect('/')


# TODO logging
@login_required
@csrf_protect
def change_password(request):
    """
    :param request:
    :return: <Return 901 if Old and new passwords match;
             Return 902 if Password was changed;
             Return 903 if Passwords don't match;>
    """
    acc = request.user
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)

    if 'old_password' and 'new_password' in body:
        old_password = body['old_password']
        if request.user.check_password(old_password):
            new_password = body['new_password']

            if old_password == new_password:
                log.debug('Old and new passwords match account {}'
                          .format(acc))
                return HttpResponse('901')

            account = Account.objects.get(email=request.user)
            account.set_password(new_password)
            account.save()

            log.debug('Password was changed account {}'
                      .format(acc))
            return HttpResponse('902')
        else:
            log.debug('Passwords don\'t match {}'
                      .format(acc))
            return HttpResponse('903')
    else:
        return HttpResponseBadRequest()
