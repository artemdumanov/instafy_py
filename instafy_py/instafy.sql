-- MySQL dump 10.13  Distrib 5.5.55, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: instafy
-- ------------------------------------------------------
-- Server version	5.5.55-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id_account` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(200) NOT NULL,
  `registration_date` datetime DEFAULT NULL,
  `plan` varchar(45) DEFAULT NULL,
  `checked` tinyint(1) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `trial` varchar(255) NOT NULL,
  `account_count` smallint(6) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `language` varchar(3) DEFAULT 'en',
  PRIMARY KEY (`id_account`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (30,'artem.dymanov@gmail.com','bcrypt$$2b$12$lgmm4C9ki/11cZVtsbFJlua1Mpnregs6PXZmtMB63ymnsFGE/ztGS',NULL,'',NULL,'2017-05-23 13:37:11','[\"2017/05/23\", \"2020/05/26\"]',0,0,'en');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity` (
  `id_activity` int(11) NOT NULL AUTO_INCREMENT,
  `configs` text NOT NULL,
  `status` enum('started','finished') NOT NULL DEFAULT 'started',
  `instagram_account` varchar(45) NOT NULL,
  `pid` smallint(6) NOT NULL,
  `begin_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `meta` text,
  PRIMARY KEY (`id_activity`),
  KEY `fk_activity_instagram_account1_idx` (`instagram_account`),
  CONSTRAINT `fk_activity_1` FOREIGN KEY (`instagram_account`) REFERENCES `instagram_account` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity`
--

LOCK TABLES `activity` WRITE;
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `id_activity` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('active','stopped') NOT NULL DEFAULT 'stopped',
  `status_date` datetime NOT NULL,
  `mode_id_mode` int(11) NOT NULL,
  `mode_account_id_account` int(11) NOT NULL,
  `pid` varchar(45) NOT NULL,
  PRIMARY KEY (`id_activity`,`mode_id_mode`,`mode_account_id_account`),
  KEY `fk_activity_log_mode1_idx` (`mode_id_mode`,`mode_account_id_account`),
  CONSTRAINT `fk_activity_log_mode1` FOREIGN KEY (`mode_id_mode`, `mode_account_id_account`) REFERENCES `mode` (`id_mode`, `account_id_account`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add activity log',1,'add_activitylog'),(2,'Can change activity log',1,'change_activitylog'),(3,'Can delete activity log',1,'delete_activitylog'),(4,'Can add instagram account',2,'add_instagramaccount'),(5,'Can change instagram account',2,'change_instagramaccount'),(6,'Can delete instagram account',2,'delete_instagramaccount'),(7,'Can add mode',3,'add_mode'),(8,'Can change mode',3,'change_mode'),(9,'Can delete mode',3,'delete_mode'),(10,'Can add proxy',4,'add_proxy'),(11,'Can change proxy',4,'change_proxy'),(12,'Can delete proxy',4,'delete_proxy'),(13,'Can add account',5,'add_account'),(14,'Can change account',5,'change_account'),(15,'Can delete account',5,'delete_account'),(16,'Can add log entry',6,'add_logentry'),(17,'Can change log entry',6,'change_logentry'),(18,'Can delete log entry',6,'delete_logentry'),(19,'Can add permission',7,'add_permission'),(20,'Can change permission',7,'change_permission'),(21,'Can delete permission',7,'delete_permission'),(22,'Can add group',8,'add_group'),(23,'Can change group',8,'change_group'),(24,'Can delete group',8,'delete_group'),(25,'Can add site_content type',9,'add_contenttype'),(26,'Can change site_content type',9,'change_contenttype'),(27,'Can delete site_content type',9,'delete_contenttype'),(28,'Can add session',10,'add_session'),(29,'Can change session',10,'change_session'),(30,'Can delete session',10,'delete_session'),(31,'Can add site',11,'add_site'),(32,'Can change site',11,'change_site'),(33,'Can delete site',11,'delete_site'),(34,'Can add auth group',12,'add_authgroup'),(35,'Can change auth group',12,'change_authgroup'),(36,'Can delete auth group',12,'delete_authgroup'),(37,'Can add auth group permissions',13,'add_authgrouppermissions'),(38,'Can change auth group permissions',13,'change_authgrouppermissions'),(39,'Can delete auth group permissions',13,'delete_authgrouppermissions'),(40,'Can add auth permission',14,'add_authpermission'),(41,'Can change auth permission',14,'change_authpermission'),(42,'Can delete auth permission',14,'delete_authpermission'),(43,'Can add auth user',15,'add_authuser'),(44,'Can change auth user',15,'change_authuser'),(45,'Can delete auth user',15,'delete_authuser'),(46,'Can add auth user groups',16,'add_authusergroups'),(47,'Can change auth user groups',16,'change_authusergroups'),(48,'Can delete auth user groups',16,'delete_authusergroups'),(49,'Can add auth user user permissions',17,'add_authuseruserpermissions'),(50,'Can change auth user user permissions',17,'change_authuseruserpermissions'),(51,'Can delete auth user user permissions',17,'delete_authuseruserpermissions'),(52,'Can add django admin log',18,'add_djangoadminlog'),(53,'Can change django admin log',18,'change_djangoadminlog'),(54,'Can delete django admin log',18,'delete_djangoadminlog'),(55,'Can add django site_content type',19,'add_djangocontenttype'),(56,'Can change django site_content type',19,'change_djangocontenttype'),(57,'Can delete django site_content type',19,'delete_djangocontenttype'),(58,'Can add django migrations',20,'add_djangomigrations'),(59,'Can change django migrations',20,'change_djangomigrations'),(60,'Can delete django migrations',20,'delete_djangomigrations'),(61,'Can add django session',21,'add_djangosession'),(62,'Can change django session',21,'change_djangosession'),(63,'Can delete django session',21,'delete_djangosession'),(64,'Can add django site',22,'add_djangosite'),(65,'Can change django site',22,'change_djangosite'),(66,'Can delete django site',22,'delete_djangosite'),(67,'Can add user',23,'add_user'),(68,'Can change user',23,'change_user'),(69,'Can delete user',23,'delete_user');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_account_id_account` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_account_id_account` FOREIGN KEY (`user_id`) REFERENCES `account` (`id_account`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (6,'admin','logentry'),(8,'auth','group'),(7,'auth','permission'),(23,'auth','user'),(9,'contenttypes','contenttype'),(5,'models','account'),(1,'models','activitylog'),(12,'models','authgroup'),(13,'models','authgrouppermissions'),(14,'models','authpermission'),(15,'models','authuser'),(16,'models','authusergroups'),(17,'models','authuseruserpermissions'),(18,'models','djangoadminlog'),(19,'models','djangocontenttype'),(20,'models','djangomigrations'),(21,'models','djangosession'),(22,'models','djangosite'),(2,'models','instagramaccount'),(3,'models','mode'),(4,'models','proxy'),(10,'sessions','session'),(11,'sites','site');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'models','0001_initial','2017-04-03 09:08:42'),(2,'contenttypes','0001_initial','2017-04-03 09:08:42'),(3,'admin','0001_initial','2017-04-03 09:08:43'),(4,'admin','0002_logentry_remove_auto_add','2017-04-03 09:08:43'),(5,'contenttypes','0002_remove_content_type_name','2017-04-03 09:08:44'),(6,'auth','0001_initial','2017-04-03 09:08:45'),(7,'auth','0002_alter_permission_name_max_length','2017-04-03 09:08:46'),(8,'auth','0003_alter_user_email_max_length','2017-04-03 09:08:46'),(9,'auth','0004_alter_user_username_opts','2017-04-03 09:08:46'),(10,'auth','0005_alter_user_last_login_null','2017-04-03 09:08:46'),(11,'auth','0006_require_contenttypes_0002','2017-04-03 09:08:46'),(12,'auth','0007_alter_validators_add_error_messages','2017-04-03 09:08:46'),(13,'auth','0008_alter_user_username_max_length','2017-04-03 09:08:46'),(14,'sessions','0001_initial','2017-04-03 09:08:46'),(15,'sites','0001_initial','2017-04-03 09:08:47'),(16,'sites','0002_alter_domain_unique','2017-04-03 09:08:47'),(17,'models','0002_authgroup_authgrouppermissions_authpermission_authuser_authusergroups_authuseruserpermissions_django','2017-04-03 09:09:18');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('0062tk7z8jtyg39itut1ebevfxkbvk3z','ZDgzNGFlNGQ2NWQyMmMyOTY0OWU1YjQ5MjdhMGViMjY0NGMxYzM2MDp7Il9hdXRoX3VzZXJfaWQiOiIyNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlZjA4ODRmNDdjOTlkYjE4MGZhM2E1M2I2YjE2ODg5NTg4ZTk1OWMxIn0=','2017-04-24 07:31:45'),('1fhf9mgsm7m42x3k1cce3y8p79xgtxx1','NDA1YjYxZDY5NzRhNmRhMDZlM2ViY2IxMzExZDQ2ZmIyYWIyOTg3ODp7Il9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiMjkiLCJfYXV0aF91c2VyX2hhc2giOiI5NzRiMTk2Njc4OTA2ZjkyNzZmMmI1NWI2M2VlYzM2NDM4NjI3NjI5In0=','2017-05-16 14:54:29'),('23qddduhbvdumeumqt7cfjglm9xnnrfq','NDg5ZTJiZmZjYzBjNmQ2MmQ4NWNhY2JiMGJhMTVjZDdlZTkxNWU4OTp7ImN1cnJlbnRfYWNjb3VudCI6Imd1di52b3AiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJtb2RlbHMuYmFja2VuZC5DdXN0b21CYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiIyM2ZkMWY5OGE1N2ZlNzQxYjdhNjBhNjJiYzk5ZTNmMDdjNmI1MzE4In0=','2017-05-05 16:27:11'),('3cva721bx9d6cv0itloa6cl80egdk7ye','MTUxMzc3NGRjZTA0OWI3MmM1NTVlMjFkZjVlOGUxNjdmNTI2NWY2ZTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3OThhNmE1Yzc2NmMzNTE4NjliOTVjM2RmNWU1MmQyNThmYTA3NzUiLCJjdXJyZW50X2FjY291bnQiOiJyZWRpYmxlLmlvIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJtb2RlbHMuYmFja2VuZC5DdXN0b21CYWNrZW5kIn0=','2017-05-07 13:11:25'),('3zf3t9fa9gez8op4jaae9i4nqpfgfbg1','YmNlNWQ0NDUyNDZjYjg2NDFiZGQ3YjZlMGFhOGYyMTQ5MmM1YjljZDp7Il9hdXRoX3VzZXJfaGFzaCI6IjIzZmQxZjk4YTU3ZmU3NDFiN2E2MGE2MmJjOTllM2YwN2M2YjUzMTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJtb2RlbHMuYmFja2VuZC5DdXN0b21CYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2017-04-24 12:54:54'),('4gwbjq0ud3ilyop8zetht6l9hvdzqlfk','NDA1YjYxZDY5NzRhNmRhMDZlM2ViY2IxMzExZDQ2ZmIyYWIyOTg3ODp7Il9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiMjkiLCJfYXV0aF91c2VyX2hhc2giOiI5NzRiMTk2Njc4OTA2ZjkyNzZmMmI1NWI2M2VlYzM2NDM4NjI3NjI5In0=','2017-05-16 14:54:18'),('57r64l1y18dpg6pgu39wz3zlcp1ig143','ZjMxOTg3YjFjNmQyMGZmZGFkNDJlNWQzNmFmMjkxZDBkZDM4NThhYTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiMjUiLCJfYXV0aF91c2VyX2hhc2giOiJmYzVhNGZhY2RmYWYyNWIxY2Y2ODFhNDNiYTUzNjBlZWFkNzMwY2M2In0=','2017-04-24 07:31:05'),('6bmrj78xtgjedroz8iinle6l08vbaj7n','OTVlMmVjM2E2YTkwNGQ2Y2RmMDg0NTM2Yzk3NDljMDkxMDM1NTEwMTp7ImN1cnJlbnRfYWNjb3VudCI6Imd1di52b3AiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3OThhNmE1Yzc2NmMzNTE4NjliOTVjM2RmNWU1MmQyNThmYTA3NzUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJtb2RlbHMuYmFja2VuZC5DdXN0b21CYWNrZW5kIn0=','2017-05-06 18:53:17'),('958hup9tmjmno9vm9cdt6gx289yvbo9f','ZmIwNDFkNmUxOGM2YmMxMWQ4ZWI0NWQwY2VmMWE5NjA0MTM4OWE3YTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoibW9kZWxzLmJhY2tlbmQuQ3VzdG9tQmFja2VuZCIsImN1cnJlbnRfYWNjb3VudCI6Imd1di52b3AiLCJfYXV0aF91c2VyX2hhc2giOiI4Nzk4YTZhNWM3NjZjMzUxODY5Yjk1YzNkZjVlNTJkMjU4ZmEwNzc1In0=','2017-05-10 21:20:20'),('9cdzil1fricazifty12eyqscyifs4oxd','ZWUxOGUwOWZmMTQwOWJjMDEyNThkZWNiYmE3NDg0MjdhYTE0YmEzYzp7ImN1cnJlbnRfYWNjb3VudCI6InMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJtb2RlbHMuYmFja2VuZC5DdXN0b21CYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMjNmZDFmOThhNTdmZTc0MWI3YTYwYTYyYmM5OWUzZjA3YzZiNTMxOCIsImN1cnJlbnQtYWNjb3VudCI6InMiLCJfYXV0aF91c2VyX2lkIjoiMSJ9','2017-04-25 20:03:36'),('9hesyp0rwe8gbshm4t81f6kbm34jflbq','ZGEzODFiM2FlOTkyYTYxYWI0YTY0ZjE1OTc5YTY2MWJkOTMwZDBlZDp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3OThhNmE1Yzc2NmMzNTE4NjliOTVjM2RmNWU1MmQyNThmYTA3NzUiLCJjdXJyZW50X2FjY291bnQiOiJndXYudm9wIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJtb2RlbHMuYmFja2VuZC5DdXN0b21CYWNrZW5kIn0=','2017-05-11 19:12:07'),('9tdq4k2m78oa8a8j8jhzk1p9gipjk07p','MjdiZTM2YmE1NDc2YTU3NWM5Y2FhNGZiZjQ0ZTliYTkzMjdjODMwYTp7ImN1cnJlbnRfYWNjb3VudCI6Imd1di52b3AiLCJfYXV0aF91c2VyX2hhc2giOiI4Nzk4YTZhNWM3NjZjMzUxODY5Yjk1YzNkZjVlNTJkMjU4ZmEwNzc1IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoibW9kZWxzLmJhY2tlbmQuQ3VzdG9tQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2017-05-24 09:52:37'),('acpga40q5nqxpea3zum5a4z7pyfb7etd','NDQ5ODMwZTlhZDcwNjA4ZWZlMTU3YzAzYWQ4MzA1YzRkMmU0OWRlMDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiODc5OGE2YTVjNzY2YzM1MTg2OWI5NWMzZGY1ZTUyZDI1OGZhMDc3NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJjdXJyZW50X2FjY291bnQiOiJndXYudm9wIn0=','2017-05-12 18:21:35'),('bejf2q0k2x6uz7a0v4cztw85p9njweqp','ODMwMTY0OWMxMzFjZDMyMWI5MzVmYzk1ZDBiNWQyNDQ2YTFhMDhkNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjIzZmQxZjk4YTU3ZmU3NDFiN2E2MGE2MmJjOTllM2YwN2M2YjUzMTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJtb2RlbHMuYmFja2VuZC5DdXN0b21CYWNrZW5kIiwiY3VycmVudF9hY2NvdW50IjoicyIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2017-04-25 20:55:16'),('cbttn2g7zhfgx8jyconf4h2u0kx40gmx','MDU1OTAxODliYzFhMjIwZGFlNWM2N2U5OTIxNWEwMDQ5NmViYTNjYzp7Il9hdXRoX3VzZXJfaWQiOiIyNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIzOGYxNWU4NjZhNmNmMmIyMDUxZDE0ZThkYTlkZWVmM2E4YWU4MzAxIn0=','2017-04-24 07:30:17'),('csej5zlbcg70gazop1xhwffjtioqfcty','NDQ5ODMwZTlhZDcwNjA4ZWZlMTU3YzAzYWQ4MzA1YzRkMmU0OWRlMDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiODc5OGE2YTVjNzY2YzM1MTg2OWI5NWMzZGY1ZTUyZDI1OGZhMDc3NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJjdXJyZW50X2FjY291bnQiOiJndXYudm9wIn0=','2017-05-09 09:03:49'),('e3rcph9xx4gw9rp82c0xsr6ck6p2it0u','OTVlMmVjM2E2YTkwNGQ2Y2RmMDg0NTM2Yzk3NDljMDkxMDM1NTEwMTp7ImN1cnJlbnRfYWNjb3VudCI6Imd1di52b3AiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3OThhNmE1Yzc2NmMzNTE4NjliOTVjM2RmNWU1MmQyNThmYTA3NzUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJtb2RlbHMuYmFja2VuZC5DdXN0b21CYWNrZW5kIn0=','2017-05-11 18:49:56'),('eowt240wnzjg9ozoask31zib05epfs32','ZGVkZDIxNTJjZDU2ZWI3ZDE3OTBjODJlYTJkYTc3NmRkMWYyYjdjMzp7Il9hdXRoX3VzZXJfaWQiOiIyOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MDljNTMzYTU0Yjk3NGY4MmYwZWFkODIwNjYzZDllMzk3ZmY3NWFmIn0=','2017-04-24 07:54:47'),('eseylur387dyzex13xjvbaibigb3vjvg','NDc3MWM0ZGIwZWU0ODZiMGRjYmM1NDMzNWNlN2Q5M2U1MmJlMzg1Mjp7Il9hdXRoX3VzZXJfaWQiOiIyMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3ZDQyMDM5MjYwYjkzMTliY2IzNzdkMjU0MjdkMjUzMzRlNzA3YWE3In0=','2017-04-24 07:26:18'),('exr62l0bsyzm5q8swlex0x7qu48ji1ib','YmEyODRjYTk3Mzg3ZmFhOWFhNTQ3ODY5NjFjMDUyYTNmNjI0ZDdjYzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJjdXJyZW50X2FjY291bnQiOiJzIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiIyM2ZkMWY5OGE1N2ZlNzQxYjdhNjBhNjJiYzk5ZTNmMDdjNmI1MzE4In0=','2017-04-26 13:21:58'),('fyotnqhsnhjov6t7exap7lwz8zg5jgiz','MDY2YmVkMGMxNDcyZWEzMDNjZTgwMTBlNTU1NzhiMDRjNjUzMmIxOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfaGFzaCI6IjIzZmQxZjk4YTU3ZmU3NDFiN2E2MGE2MmJjOTllM2YwN2M2YjUzMTgifQ==','2017-04-24 09:18:15'),('gq254setn0dcvnvsvd1939d3ln8awnmn','NDQ5ODMwZTlhZDcwNjA4ZWZlMTU3YzAzYWQ4MzA1YzRkMmU0OWRlMDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiODc5OGE2YTVjNzY2YzM1MTg2OWI5NWMzZGY1ZTUyZDI1OGZhMDc3NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJjdXJyZW50X2FjY291bnQiOiJndXYudm9wIn0=','2017-05-07 13:20:23'),('gx1kk753xkh34e1d2thqoklsbkxmjpa4','YmNlNWQ0NDUyNDZjYjg2NDFiZGQ3YjZlMGFhOGYyMTQ5MmM1YjljZDp7Il9hdXRoX3VzZXJfaGFzaCI6IjIzZmQxZjk4YTU3ZmU3NDFiN2E2MGE2MmJjOTllM2YwN2M2YjUzMTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJtb2RlbHMuYmFja2VuZC5DdXN0b21CYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2017-04-24 10:05:36'),('i052glx37aet6n45454gbr0rlj8k6t62','NDgzYjgxMDc4NWExYTJhZTE3Mjk3MzdmMzdjYTEyNzMwZmJlZWIwODp7ImFjY291bnRzX2V4cGlyeSI6eyJndXYudm9wIjoiMjAxNy0wNi0wMiAxOTowNTo0NC40NTYxNjgifSwiX2F1dGhfdXNlcl9oYXNoIjoiODJkNzA1NzhlNjMyYWJhY2NiNTJlMDkyNmQ4ZDcxN2ZiNjFlMmJmMCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiMzAiLCJjdXJyZW50X2FjY291bnQiOiJndXYudm9wIn0=','2017-06-16 19:05:55'),('ii7idrdbw8augda9nshpfej7hw8e34lz','NmZjNDFkNTI0MjE3OGZmOWM4NzU4NjVlZjIzN2U0NmJkZjg5ZmUzMzp7fQ==','2017-04-24 07:25:44'),('invbhepn3rvts4fjawatzabx8zqxfxlm','MzExYTQ3ZGY2MTI2NTY1ZDViMWI5MjRlNzQzYWViMTk5N2QzODI2Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJjdXJyZW50X2FjY291bnQiOiJndXYudm9wIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI4Nzk4YTZhNWM3NjZjMzUxODY5Yjk1YzNkZjVlNTJkMjU4ZmEwNzc1In0=','2017-05-09 20:54:14'),('lgi9y9cuw0o54zj7m1rtcmk8bzcyxeex','YjE4ZTM5ZDQwODUwNGE0ZDNlZTU1MTE4NDU4MmE0NWNkMTU5YmQ0OTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiMjNmZDFmOThhNTdmZTc0MWI3YTYwYTYyYmM5OWUzZjA3YzZiNTMxOCIsImN1cnJlbnRfYWNjb3VudCI6InMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJtb2RlbHMuYmFja2VuZC5DdXN0b21CYWNrZW5kIn0=','2017-04-25 21:11:41'),('may5wy2w5jng6d0yw3pd397meozo2mjs','MzM1ZGIwZWFmZTA5MjRiMmIzZjNhNzBhNGVkYWQ0ZWNmZjE0NGY1NTp7Il9hdXRoX3VzZXJfaWQiOiIyMyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiYTk4YzQwYmRkOTk2MzM3ZmE5Nzc0ZDUwN2Y4MWI5MmJkMWYxNmJjIn0=','2017-04-24 07:29:11'),('nfjhbga1zs7kd23j0zu67o5wporn4439','NGIxMzliNjUwN2QxY2JhMmYzNTIzM2MyMWMzOGE3N2ExNDkxODQ0ODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiMjNmZDFmOThhNTdmZTc0MWI3YTYwYTYyYmM5OWUzZjA3YzZiNTMxOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQifQ==','2017-04-25 20:22:32'),('ryxor1luzjtn3iz8iigarn9zppuuhepc','YmE3ZGEwODk4YThjZDY2NGFmNDI0ODc5OWM5YzhlZDNlNmQxNDVjNTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiY3VycmVudF9hY2NvdW50IjoiZ3V2LnZvcCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyM2ZkMWY5OGE1N2ZlNzQxYjdhNjBhNjJiYzk5ZTNmMDdjNmI1MzE4In0=','2017-05-02 18:03:18'),('sfia6acnvqnmqw4nd02uynlwcn54jt6t','M2RhNjkxY2M0N2Q2ZjE5MTQwMDYyN2U0YWFkMjQxY2EwYmJhNWJlNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiMSIsImN1cnJlbnRfYWNjb3VudCI6Imd1di52b3AiLCJfYXV0aF91c2VyX2hhc2giOiI4Nzk4YTZhNWM3NjZjMzUxODY5Yjk1YzNkZjVlNTJkMjU4ZmEwNzc1In0=','2017-05-07 15:53:50'),('sut84dpccproxcd9rx8cpjbzw99i62xj','YjhhMGVmMWVkZjZmYjkxYmUxMDk0ZTQ2YzNlMTY4NzRmZDY0NTRmNTp7ImN1cnJlbnRfYWNjb3VudCI6InJlZGlibGUuaW8iLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3OThhNmE1Yzc2NmMzNTE4NjliOTVjM2RmNWU1MmQyNThmYTA3NzUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJtb2RlbHMuYmFja2VuZC5DdXN0b21CYWNrZW5kIn0=','2017-05-08 09:34:56'),('tm3jlzv44rhzpliarhnademiwkfan8oq','MDcxZmZjNjFiYTRjMDhhOTAzNTE5ZTQ4YTEyODhhMTUwM2NlNDVjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJjdXJyZW50X2FjY291bnQiOiJndXYudm9wIiwiX2F1dGhfdXNlcl9oYXNoIjoiODc5OGE2YTVjNzY2YzM1MTg2OWI5NWMzZGY1ZTUyZDI1OGZhMDc3NSIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2017-05-12 05:54:38'),('tm7qkl49syjkwd9hxyg9dv8l5hdjl1q6','Yjg5ODVlYTA2OTU4NzJhY2RlNmQ2NmRlMDQ4OWY1MGVmMDMyNWFkOTp7Il9hdXRoX3VzZXJfaWQiOiIyNyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIwM2QxZDJjYmUwODI1YmY3NGQ1YWVhYzY2MWFkNzcwZWVhM2YzMjNhIn0=','2017-04-24 07:54:03'),('v2ghzfwn0mfbshl9vlwhfknzeve2xnmf','NDQ5ODMwZTlhZDcwNjA4ZWZlMTU3YzAzYWQ4MzA1YzRkMmU0OWRlMDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiODc5OGE2YTVjNzY2YzM1MTg2OWI5NWMzZGY1ZTUyZDI1OGZhMDc3NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJjdXJyZW50X2FjY291bnQiOiJndXYudm9wIn0=','2017-05-09 09:06:06'),('v8aaoovbrbv86su1e6792vv6533e84sf','YWJkZDA1NzIzOTcyMzJmMmIxMDhhNGI2NTU4OTU4ZDdhYTdlOWRmMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJjdXJyZW50X2FjY291bnQiOiJfZHVtYW5vdiIsIl9hdXRoX3VzZXJfaGFzaCI6Ijk3NGIxOTY2Nzg5MDZmOTI3NmYyYjU1YjYzZWVjMzY0Mzg2Mjc2MjkiLCJfYXV0aF91c2VyX2lkIjoiMjkifQ==','2017-05-17 09:43:32'),('vi3ihcsq3cj1idtbadpe0dwnwu548ido','N2I1Y2I5YjcwMjM0ZDZiZmRkNTdhY2VjY2RlNDAzY2Y4NDc5ZWI4ZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjIzZmQxZjk4YTU3ZmU3NDFiN2E2MGE2MmJjOTllM2YwN2M2YjUzMTgiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6Im1vZGVscy5iYWNrZW5kLkN1c3RvbUJhY2tlbmQiLCJjdXJyZW50X2FjY291bnQiOiJndXYudm9wIn0=','2017-05-03 17:31:09'),('xugo9kc1jq4hft215634qnd0iihc4z43','MzZhNDEzNTZhODZjY2I2MDkwNWNmNDNiNjk4MmFhMzcyZjA0ZjAyMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjYxZmI3NWUwMGU2MTNlNzdjOTEyNWMzZTUzODRkZTdhNmViYzMyMjQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJtb2RlbHMuYmFja2VuZC5DdXN0b21CYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIwIn0=','2017-04-24 07:23:30');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_site_domain_a2e37b91_uniq` (`domain`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id_feedbacks` bigint(20) NOT NULL AUTO_INCREMENT,
  `feedback` text NOT NULL,
  `date` datetime DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `type` enum('feedback','report') NOT NULL DEFAULT 'feedback',
  PRIMARY KEY (`id_feedbacks`),
  KEY `fk_feedback_1_idx` (`account`),
  CONSTRAINT `fk_feedback_1` FOREIGN KEY (`account`) REFERENCES `account` (`id_account`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES (1,'adsasdas','2017-04-20 10:30:17',NULL,'feedback'),(2,'adsasd','2017-04-20 11:43:28',NULL,'report');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instagram_account`
--

DROP TABLE IF EXISTS `instagram_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instagram_account` (
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `account_id_account` int(11) NOT NULL,
  `expired_date` datetime NOT NULL,
  `history_of_payments` text,
  `account_type` enum('trial','basic','advanced') DEFAULT 'trial',
  PRIMARY KEY (`username`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_instagram_account_account_idx` (`account_id_account`),
  CONSTRAINT `account_id_account` FOREIGN KEY (`account_id_account`) REFERENCES `account` (`id_account`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instagram_account`
--

LOCK TABLES `instagram_account` WRITE;
/*!40000 ALTER TABLE `instagram_account` DISABLE KEYS */;
INSERT INTO `instagram_account` VALUES ('guv.vop','a6n12a8g4',30,'2017-06-05 12:31:39',NULL,'trial');
/*!40000 ALTER TABLE `instagram_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mode`
--

DROP TABLE IF EXISTS `mode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mode` (
  `id_mode` int(11) NOT NULL AUTO_INCREMENT,
  `mode_name` varchar(45) NOT NULL,
  `likes_per_hour` smallint(5) unsigned DEFAULT '20',
  `comments_per_hour` smallint(5) unsigned DEFAULT '0',
  `tag_list` text,
  `tag_blacklist` text,
  `user_blacklist` text,
  `max_like_for_one_tag` smallint(5) unsigned DEFAULT '10',
  `follows_per_hour` smallint(5) unsigned DEFAULT '20',
  `follow_time` int(10) unsigned DEFAULT '432000',
  `log_mod` enum('0','1','2','3','4','5') DEFAULT '0',
  `account_id_account` int(11) NOT NULL,
  `active_hours` varchar(145) DEFAULT '0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23',
  `registration_date` datetime NOT NULL,
  `unfollow_per_hour` smallint(5) unsigned DEFAULT '0',
  `unfollow_break_min` int(10) unsigned DEFAULT '30',
  `unfollow_break_max` int(10) unsigned DEFAULT '60',
  `media_min_like` int(10) unsigned DEFAULT '5',
  `media_max_like` int(10) unsigned DEFAULT '500',
  `comment_list` text,
  PRIMARY KEY (`id_mode`,`account_id_account`),
  KEY `fk_mode_account1_idx` (`account_id_account`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mode`
--

LOCK TABLES `mode` WRITE;
/*!40000 ALTER TABLE `mode` DISABLE KEYS */;
INSERT INTO `mode` VALUES (1,'q',1,1,'1','',NULL,10,20,432000,'0',1,'0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23','0000-00-00 00:00:00',0,30,60,5,500,NULL),(2,'sa',10,0,NULL,NULL,NULL,10,20,432000,'0',1,'0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23','0000-00-00 00:00:00',0,30,60,5,500,NULL),(3,'asda',10,0,NULL,NULL,NULL,10,20,432000,'0',1,'0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23','0000-00-00 00:00:00',0,30,60,5,500,NULL),(4,'sddsfggg',10,0,NULL,NULL,NULL,10,20,432000,'0',1,'0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23','0000-00-00 00:00:00',0,30,60,5,500,NULL);
/*!40000 ALTER TABLE `mode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proxy`
--

DROP TABLE IF EXISTS `proxy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proxy` (
  `id_proxy` int(11) NOT NULL AUTO_INCREMENT,
  `proxy` varchar(45) NOT NULL,
  `instagram_account_account_id_account` varchar(45) NOT NULL,
  `meta` text,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `country` varchar(45) NOT NULL,
  `period` int(4) unsigned NOT NULL,
  `price` float unsigned NOT NULL,
  `proxy_provider_id` int(10) unsigned NOT NULL,
  `ip` varchar(45) NOT NULL,
  PRIMARY KEY (`id_proxy`),
  UNIQUE KEY `instagram_account_account_id_account_UNIQUE` (`instagram_account_account_id_account`),
  KEY `fk_proxy_instagram_account1_idx` (`instagram_account_account_id_account`),
  CONSTRAINT `instagram_account_account_id_account` FOREIGN KEY (`instagram_account_account_id_account`) REFERENCES `instagram_account` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proxy`
--

LOCK TABLES `proxy` WRITE;
/*!40000 ALTER TABLE `proxy` DISABLE KEYS */;
INSERT INTO `proxy` VALUES (1,'d1thtu:2nY9cE@107.191.105.212:17166','guv.vop',NULL,'0000-00-00 00:00:00',NULL,'',0,0,0,'');
/*!40000 ALTER TABLE `proxy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proxy_for_refund`
--

DROP TABLE IF EXISTS `proxy_for_refund`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proxy_for_refund` (
  `id_proxy` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proxy` varchar(45) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `finish_date` datetime NOT NULL,
  `account` varchar(45) NOT NULL,
  `meta` text,
  `country` varchar(45) NOT NULL,
  `period` int(4) unsigned NOT NULL,
  `price` float unsigned NOT NULL,
  `proxy_provider_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_proxy`),
  KEY `account` (`account`),
  CONSTRAINT `proxy_for_refund_ibfk_1` FOREIGN KEY (`account`) REFERENCES `instagram_account` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proxy_for_refund`
--

LOCK TABLES `proxy_for_refund` WRITE;
/*!40000 ALTER TABLE `proxy_for_refund` DISABLE KEYS */;
/*!40000 ALTER TABLE `proxy_for_refund` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-02 23:16:17
