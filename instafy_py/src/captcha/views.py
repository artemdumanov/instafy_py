import json

import requests
from django.http import HttpResponseBadRequest
from django.http import JsonResponse

from utils import logging

log = logging.getLogger()

def signup_captcha(request):
    try:
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)

        if 'response' in body:
            response = body['response']

            req = requests.post('https://www.google.com/recaptcha/api/siteverify', {
                'secret': '6LfvqR8UAAAAAI7hCYhdNx-H1fHE4H0aJ0r6vi4G',
                'response': response,
            })

            status = req.json()["success"]

            resp = json.loads('{\"status\": ' + str(status).lower() + '}')

            return JsonResponse(resp, safe=False)

        return HttpResponseBadRequest()

    except Exception as e:
        raise
