from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt

from captcha import views

urlpatterns = [
    url(r'check/signup/$', views.signup_captcha, name="signup_captcha")
]
