from django.apps import AppConfig


class AcquiringConfig(AppConfig):
    name = 'acquiring'
