from django.conf.urls import url

from acquiring import views

app_name = 'acquiring'
urlpatterns =[
    url(r'callback/$', views.callback, name="callback"),
    url(r'checkout/$', views.Checkout.as_view(), name="checkout"),
    url(r'success/$', views.SuccessAcquiring.as_view(), name="success"),
]
