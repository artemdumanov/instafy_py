# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.contrib.auth.base_user import AbstractBaseUser
from django.core.validators import MinValueValidator
from django.db import models

from models.managers import AccountManager


class Account(AbstractBaseUser):
    # class Account(models.Model):
    id_account = models.AutoField(primary_key=True)
    email = models.CharField(max_length=45, unique=True)
    password = models.CharField(max_length=45)
    registration_date = models.DateTimeField()
    plan = models.CharField(max_length=45)
    checked = models.IntegerField()
    last_login = models.DateTimeField(auto_now_add=True)
    trial = models.CharField(null=False, blank=False, max_length=255)
    # trial_end_time = models.TimeField(blank=False, null=False)
    account_count = models.PositiveSmallIntegerField(null=False)
    is_active = models.IntegerField()
    language = models.CharField(max_length=3)

    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['plan', 'registration_date', 'checked']

    class Meta:
        managed = False
        db_table = 'account'


class ActivityLog(models.Model):
    id_activity = models.AutoField(primary_key=True)
    status = models.CharField(max_length=6)
    status_date = models.DateTimeField()
    mode_id_mode = models.ForeignKey('Mode', models.DO_NOTHING, db_column='mode_id_mode', related_name='mode_id_mode')
    mode_account_id_account = models.ForeignKey('Mode', models.DO_NOTHING, db_column='mode_account_id_account',
                                                related_name='mode_account_id_account')

    class Meta:
        managed = False
        db_table = 'activity_log'
        unique_together = (('id_activity', 'mode_id_mode', 'mode_account_id_account'),)


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DjangoSite(models.Model):
    domain = models.CharField(unique=True, max_length=100)
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'django_site'


# TODO registration_date
class InstagramAccount(models.Model):
    username = models.CharField(max_length=45, primary_key=True)
    password = models.CharField(max_length=45)
    account_id_account = models.ForeignKey(Account, models.DO_NOTHING, db_column='account_id_account')
    expired_date = models.DateTimeField(null=False)
    history_of_payments = models.TextField(blank=True, null=True)
    account_type = models.TextField(default='trial')

    class Meta:
        managed = False
        db_table = 'instagram_account'
        unique_together = (('account_id_account', 'username'),)


class Mode(models.Model):
    id_mode = models.AutoField(primary_key=True)
    mode_name = models.CharField(max_length=45)
    likes_per_hour = models.SmallIntegerField(blank=True, null=True)
    comments_per_hour = models.SmallIntegerField(blank=True, null=True)
    tag_list = models.TextField(blank=True, null=True)
    tag_blacklist = models.TextField(blank=True, null=True)
    user_blacklist = models.TextField(blank=True, null=True)
    max_like_for_one_tag = models.SmallIntegerField(blank=True, null=True)
    follows_per_hour = models.SmallIntegerField(blank=True, null=True)
    follow_time = models.IntegerField(blank=True, null=True)
    log_mod = models.CharField(max_length=1, blank=True, null=True)
    account_id_account = models.ForeignKey(Account, models.DO_NOTHING, db_column='account_id_account')
    active_hours = models.CharField(max_length=145, blank=True, null=True)
    registration_date = models.DateTimeField()
    unfollow_per_hour = models.SmallIntegerField(blank=True, null=True)
    unfollow_break_min = models.IntegerField(blank=True, null=True)
    unfollow_break_max = models.IntegerField(blank=True, null=True)
    media_min_like = models.IntegerField(blank=True, null=True)
    media_max_like = models.IntegerField(blank=True, null=True)
    comment_list = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mode'
        unique_together = (('id_mode', 'account_id_account'),)


class Proxy(models.Model):
    id_proxy = models.AutoField(primary_key=True)
    proxy = models.CharField(max_length=45, blank=False, null=False)
    meta = models.TextField(null=True)
    start_date = models.DateTimeField(null=False)
    end_date = models.DateTimeField(null=True)
    country = models.CharField(max_length=45, null=False)
    period = models.PositiveIntegerField()
    price = models.FloatField(validators=[MinValueValidator(0.0), ])
    # It can confuse, but our proxy provider have it own id for proxy
    proxy_provider_id = models.PositiveIntegerField()
    ip = models.CharField(max_length=45, null=False, blank=False)
    account = models.ForeignKey(InstagramAccount, models.DO_NOTHING,
                                db_column='instagram_account_account_id_account')

    class Meta:
        managed = False
        db_table = 'proxy'
        unique_together = (('id_proxy', 'account'),)


class ProxyForRefund(models.Model):
    id_proxy = models.AutoField(primary_key=True)
    account = models.ForeignKey(InstagramAccount, models.DO_NOTHING, db_column='account')
    start_date = models.DateField(null=True)
    finish_date = models.DateField(null=False)
    country = models.CharField(max_length=45, null=False)
    period = models.PositiveIntegerField()
    price = models.FloatField(validators=[MinValueValidator(0.0), ])
    # It can confuse, but our proxy provider have it own id for proxy
    proxy_provider_id = models.PositiveIntegerField()
    meta = models.TextField(null=False)

    class Meta:
        managed = False
        db_table = 'proxy_for_refund'


class Activity(models.Model):
    id_activity = models.AutoField(primary_key=True)
    configs = models.TextField(null=False)
    pid = models.PositiveSmallIntegerField()
    instagram_account = models.ForeignKey(InstagramAccount, models.DO_NOTHING, db_column='instagram_account')
    status = models.TextField()
    begin_date = models.DateField()
    end_date = models.DateField()
    creation_date = models.DateTimeField()
    meta = models.TextField()

    class Meta:
        managed = False
        db_table = 'activity'


class Feedback(models.Model):
    id_feedback = models.AutoField(primary_key=True)
    feedback = models.TextField(null=False)
    account = models.ForeignKey(Account, models.DO_NOTHING, db_column='account', null=False)
    date = models.DateTimeField()
    type = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'feedback'
