from django.test import TestCase
from django.utils import timezone

from models.models import Account

class CreateUserTestCase(TestCase):
    def setUp(self):
        account = Account()
        account.registration_date = timezone.now()
        account.expired_date = timezone.now()
        account.plan = 'trial'
        account.checked = 0

        account.email = 'artem@gmail.com'
        account.set_password('12345678')
