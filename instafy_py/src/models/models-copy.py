# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.contrib.auth.models import AbstractBaseUser
from django.db import models

from models.managers import AccountManager


class Account(AbstractBaseUser):
    id_account = models.AutoField(primary_key=True)
    email = models.CharField(max_length=45, unique=True)
    password = models.CharField(max_length=45)
    registration_date = models.DateTimeField()
    plan = models.CharField(max_length=45)
    expired_date = models.DateTimeField()
    checked = models.IntegerField()

    objects = AccountManager

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['plan', 'expired_date', 'registration_date', 'checked']

    class Meta:
        managed = False
        db_table = 'account'

class InstagramAccount(models.Model):
    username = models.CharField(max_length=45, primary_key=True)
    password = models.CharField(max_length=45)
    account_id_account = models.ForeignKey(Account, models.DO_NOTHING, db_column='account_id_account')

    class Meta:
        managed = False
        db_table = 'instagram_account'
        unique_together = (('account_id_account', 'username'),)


class Mode(models.Model):
    id_mode = models.AutoField(primary_key=True)
    mode_name = models.CharField(max_length=45)
    likes_per_hour = models.SmallIntegerField(blank=True, null=True)
    comments_per_hour = models.SmallIntegerField(blank=True, null=True)
    tag_list = models.TextField(blank=True, null=True)
    tag_blacklist = models.TextField(blank=True, null=True)
    user_blacklist = models.TextField(blank=True, null=True)
    max_like_for_one_tag = models.SmallIntegerField(blank=True, null=True)
    follows_per_hour = models.SmallIntegerField(blank=True, null=True)
    follow_time = models.IntegerField(blank=True, null=True)
    log_mod = models.CharField(max_length=1, blank=True, null=True)
    account_id_account = models.ForeignKey(Account, models.DO_NOTHING, db_column='account_id_account')
    active_hours = models.CharField(max_length=145, blank=True, null=True)
    registration_date = models.DateTimeField()
    unfollow_per_hour = models.SmallIntegerField(blank=True, null=True)
    unfollow_break_min = models.IntegerField(blank=True, null=True)
    unfollow_break_max = models.IntegerField(blank=True, null=True)
    media_min_like = models.IntegerField(blank=True, null=True)
    media_max_like = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mode'
        unique_together = (('id_mode', 'account_id_account'),)


class Proxy(models.Model):
    id_proxy = models.AutoField(primary_key=True)
    proxy = models.CharField(max_length=45, blank=True, null=True)
    instagram_account_account_id_account = models.ForeignKey(InstagramAccount, models.DO_NOTHING, db_column='instagram_account_account_id_account')

    class Meta:
        managed = False
        db_table = 'proxy'
        unique_together = (('id_proxy', 'instagram_account_account_id_account'),)


class ActivityLog(models.Model):
    id_activity = models.AutoField(primary_key=True)
    status = models.CharField(max_length=6)
    status_date = models.DateTimeField()
    mode_id_mode = models.ForeignKey('Mode', models.DO_NOTHING, db_column='mode_id_mode', related_name='mode_id_mode')
    mode_account_id_account = models.ForeignKey('Mode', models.DO_NOTHING, db_column='mode_account_id_account', related_name='mode_account_id_account')

    class Meta:
        managed = False
        db_table = 'activity_log'
        unique_together = (('id_activity', 'mode_id_mode', 'mode_account_id_account'),)

