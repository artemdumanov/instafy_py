from django.conf.urls import include
from django.conf.urls import url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from site_content.views import *

urlpatterns = [
    url(r'^$', LandingView.as_view(), name="landing"),
    url(r'^pricing/$', PricingView.as_view()),
    url(r'^faq/$', FaqView.as_view()),
    url(r'^terms/$', TermsView.as_view()),
    url(r'^privacy/$', PrivacyView.as_view())
]
