from django.apps import AppConfig


class InstabotConfig(AppConfig):
    name = 'instabot'
