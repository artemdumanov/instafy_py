import sys

from main.settings import ROOT_DIR

sys.path.append(ROOT_DIR + '/instabot/src')
from instabot import InstaBot

from utils import logging

log = logging.getLogger()


class CustomBot(InstaBot):
    def write_log(self, log_text):
        try:
            log.info(("%s " + " %s")%(self.user_login ,log_text))
        except Exception:
            log.exception('Cant write to log '+ CustomBot.__class__.__name__)
            raise