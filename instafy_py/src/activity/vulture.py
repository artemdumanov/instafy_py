import json
import multiprocessing
import os
import random
import signal
import time
from datetime import date

from django.core.mail import send_mail
from django.db.models import Q
from django.template.loader import render_to_string
from django.utils import timezone
from psutil import Process, NoSuchProcess

from activity import controller
from models.models import Activity, Proxy, InstagramAccount
from utils import logging
from utils.pysched import PySched

log = logging.getLogger()


class Vulture():
    lock = False

    def __init__(self):
        if not Vulture.lock:
            start_p(check_running_activities)
            log.info('Init vulture active checker')

            start_p(kill_expired_activities)
            log.info('Init vulture expire checker')

            Vulture.lock = True


def check_running_activities():
    while True:
        log.info('Check running activities')
        time.sleep(60)

        activities = Activity.objects.filter(
            Q(status='started') &
            Q(instagram_account__expired_date__gte=timezone.now()))

        for activity in activities:
            check_activity(activity)


def check_activity(activity):
    pid = activity.pid
    log.info('Check running activity; pid: %d' % pid)

    try:
        process = Process(pid)
    except NoSuchProcess:
        # when i stop process, it looks like zombie and reboot
        log.info('No such process; pid: %d; activity id: %d; username: %s'
                 % (pid, activity.id_activity,
                    activity.instagram_account))
        boot_activity(activity)
        return
    else:
        with process.oneshot():
            status = process.status()

            if status == 'zombie':
                log.info('Zombie process, trying to terminate and boot '
                         'activity; pid: %d'
                         % process.pid)
                process.terminate()
                # TODO delete pid
                boot_activity(activity)


def boot_activity(activity):
    time.sleep(random.randrange(30))

    instagram = activity.instagram_account
    proxy = Proxy.objects.get(account=instagram)

    log.info('Trying to boot activity; pid: %d; activity id: %d; username: %s'
             % (activity.pid,
                activity.id_activity,
                activity.instagram_account))

    configs = json.loads(activity.configs)
    configs['instagram'] = instagram
    configs['proxy'] = proxy
    configs['password'] = activity.instagram_account.password
    configs['proxy'] = Proxy.objects.get(account=instagram)

    pid = activity.pid
    try:
        p = multiprocessing.Process(target=controller.create_bot,
                                    kwargs=configs)
        p.start()
        pid = p.pid
    except Exception as err:
        log.exception('Can\'t boot bot by vulture; pid: %d; '
                      'activity id: %d; username: %s'
                      % (activity.pid,
                         activity.id_activity,
                         activity.instagram_account.username))
        raise

    log.info('New process was started; pid: %d; activity id: %d; username: %s'
             % (activity.pid,
                activity.id_activity,
                activity.instagram_account.username))

    meta = activity.meta

    if meta == '':
        meta = {
            'zombie_state': [format_meta_zombie(timezone.now())]
        }
        log.info('Add meta zombie state; pid: %d; activity id: %d;'
                 ' username: %s' % (activity.pid,
                                    activity.id_activity,
                                    activity.instagram_accoun.usernamet))
    else:
        meta = json.loads(meta)
        meta['zombie_state'] += [format_meta_zombie(timezone.now())]
        log.info('Update meta zombie state; pid: %d; activity id: %d; '
                 'username: %s' % (activity.pid,
                                   activity.id_activity,
                                   activity.instagram_account.username))

    activity.pid = pid
    activity.meta = json.dumps(meta)

    activity.save(update_fields=['pid', 'meta'])

    log.info('Save changes to zombie; pid: %d; activity id: %d; '
             'username: %s' % (activity.pid,
                               activity.id_activity,
                               activity.instagram_account.username))


def hunt(id_activity, process):
    def __hunt(id, p):
        while True:
            activity = Activity.objects.get(id_activity=id)

            log.info('Start hunt on activity; acitivity id: %s, process: %s'
                     % (activity.id_activity, process))

            if activity.status == 'finished':
                os.kill(activity.pid, signal.SIGKILL)

            time.sleep(60)

    start_p(__hunt, {'id': id_activity, 'p': process})


def format_meta_zombie(date):
    return '%d/%d/%d %d:%d' % (date.year, date.month, date.day,
                               date.hour, date.minute)


def kill_expired_activities():
    while True:
        log.info('Check expired activities')
        activities = Activity.objects.filter(
            Q(status='started')
            & Q(instagram_account__expired_date__lte=timezone.now()))

        for activity in activities:
            try:
                p = Process(activity.pid)
            except NoSuchProcess as e:
                log.exception('When trying to kill expired activity, '
                              'no such process; pid: %d; activity id: %d; '
                              'username: %s'.format(activity.pid,
                                                    activity.id_activity,
                                                    activity.instagram_account))
                raise
            else:
                p.terminate()
                log.info('Kill expired activity; pid: %d;'
                         'activity id: %d; username: %s'
                         % (activity.pid,
                            activity.id_activity,
                            activity.instagram_account))
            finally:
                activity.status = 'finished'
                activity.save(update_fields=['status'])

                log.info('Update expired activity; pid: %d;'
                         'activity id: %d; username: %s'
                         % (activity.pid,
                            activity.id_activity,
                            activity.instagram_account))

        # once a day
        # TODO every midnight
        time.sleep(24 * 60 * 60)


def start_p(func, kwargs={}):
    p = multiprocessing.Process(target=func, kwargs=kwargs)
    p.start()


class DailyExpireManager:
    lock = False

    def __init__(self):
        if not DailyExpiredManager.lock:
            self.enable_daily_expired_accounts_check()
            DailyExpiredManager.lock = True

    def enable_daily_expired_accounts_check(self):
        PySched(func=self.expired_accounts_check(True),
                kwargs={'send_emails': True},
                hours=(0,))

    def expired_accounts_check(self, send_emails=False):
        today = date.today()
        instagrams_basic_expired = InstagramAccount.objects.filter(
            Q(expired_date__contains=today)
        )

        log.info('Check all expired accounts. Total count is {}'
                 .format(instagrams_basic_expired.count()))

        if send_emails:
            log.info('Send email for all expired accounts. Total count is {}'
                     .format(instagrams_basic_expired.count()))
            for instagram in instagrams_basic_expired:
                email = instagram.account_id_account.email
                username = instagram.username

                try:
                    message = render_to_string(
                        template_name='instagram-expired-email.html',
                        context={
                            'instagram_username': username,
                            'price': '$6.99'
                        }
                    )
                    send_mail(
                        subject='{} окончание подписки'.format(username),
                        message=message,
                        html_message=message,
                        from_email='support@ugram.co',
                        # nasty, I know
                        recipient_list=[email],
                        fail_silently=False,
                    )
                except:
                    log.exception('Can\'t send email about expired '
                                  'instagram account {} to {}'
                                  .format(username, email))
                else:
                    log.info('Successfully sent email about expired '
                             'instagram account {} to {}'
                             .format(username, email))

                    # return instagrams
