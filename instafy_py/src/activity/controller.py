#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import time

from requests.exceptions import ProxyError

from main.settings import ROOT_DIR
from utils.proxy import ProxyManager

sys.path.append(ROOT_DIR + '/instabot/src')

from check_status import check_status
from feed_scanner import feed_scanner
from follow_protocol import follow_protocol
from activity.instabot import CustomBot
from unfollow_protocol import unfollow_protocol

from utils import logging

log = logging.getLogger()


def create_bot(instagram, username, proxy, password, mode=0, like_per_day=0, comments_per_day=0, tag_list=None,
               tag_blacklist=None,
               user_blacklist=None, max_like_for_one_tag=10, follow_per_day=0, follow_time=0, unfollow_per_day=0,
               unfollow_break_min=15, unfollow_break_max=30, media_min_like=0, media_max_like=0, comment_list=None):
    proxy_manager = ProxyManager()
    try:
        bot = CustomBot(
            login=username,
            password=password,
            like_per_day=like_per_day,
            comments_per_day=comments_per_day,
            tag_list=tag_list,
            tag_blacklist=tag_blacklist,
            user_blacklist=user_blacklist,
            max_like_for_one_tag=max_like_for_one_tag,
            follow_per_day=follow_per_day,
            follow_time=follow_time,
            unfollow_per_day=unfollow_per_day,
            unfollow_break_min=unfollow_break_min,
            unfollow_break_max=unfollow_break_max,
            media_max_like=media_max_like,
            media_min_like=media_min_like,
            log_mod=0,
            comment_list=[comment_list],
            proxy=proxy.proxy,
            # Use unwanted_username_list to block usernames containing a string
            ## Will do partial matches; i.e. 'mozart' will block 'legend_mozart'
            ### 'free_followers' will be blocked because it contains 'free'
            unwanted_username_list=[
                'second', 'stuff', 'art', 'project', 'love', 'life', 'food', 'blog',
                'free', 'keren', 'photo', 'graphy', 'indo', 'travel', 'art', 'shop',
                'store', 'sex', 'toko', 'jual', 'online', 'murah', 'jam', 'kaos',
                'case', 'baju', 'fashion', 'corp', 'tas', 'butik', 'grosir', 'karpet',
                'sosis', 'salon', 'skin', 'care', 'cloth', 'tech', 'rental', 'kamera',
                'beauty', 'express', 'kredit', 'collection', 'impor', 'preloved',
                'follow', 'follower', 'gain', '.id', '_id', 'bags'
            ],
            unfollow_whitelist=['example_user_1', 'example_user_2'],
            # sleep_at=-1,
            # sleep_time=1 * 60 * 60,
            # to_cleanup=False
        )
    except ProxyError as e:
        # look for def sources, and you understand why
        proxy_provider_id = proxy.proxy_provider_id
        country = proxy.country
        period = proxy.period

        meta = {
            'proxy': proxy.proxy,
            'country': country,
            'proxy_provider_id': proxy_provider_id
        }

        proxy_manager.request_refund(instagram, proxy.proxy,
                                     proxy_provider_id, country,
                                     period, str(proxy_manager.add_meta(meta)))
        log.exception('Proxy error')
        raise
    while True:

        mode = mode

        # print("# MODE 0 = ORIGINAL MODE BY LEVPASHA")
        # print("## MODE 1 = MODIFIED MODE BY KEMONG")
        # print("### MODE 2 = ORIGINAL MODE + UNFOLLOW WHO DON'T FOLLOW BACK")
        # print("#### MODE 3 = MODIFIED MODE : UNFOLLOW USERS WHO DON'T FOLLOW YOU BASED ON RECENT FEED")
        # print("##### MODE 4 = MODIFIED MODE : FOLLOW USERS BASED ON RECENT FEED ONLY")
        # print("###### MODE 5 = MODIFIED MODE : JUST UNFOLLOW EVERYBODY, EITHER YOUR FOLLOWER OR NOT")

        ################################
        ##  WARNING   ###
        ################################

        # DON'T USE MODE 5 FOR A LONG PERIOD. YOU RISK YOUR ACCOUNT FROM GETTING BANNED
        ## USE MODE 5 IN BURST MODE, USE IT TO UNFOLLOW PEOPLE AS MANY AS YOU WANT IN SHORT TIME PERIOD


        # print("You choose mode : %i" %(mode))
        # print("CTRL + C to cancel this operation or wait 30 seconds to start")
        # time.sleep(30)

        if mode == 0:
            bot.new_auto_mod()

        elif mode == 1:
            check_status(bot)
            while bot.self_following - bot.self_follower > 200:
                unfollow_protocol(bot)
                time.sleep(10 * 60)
                check_status(bot)
            while bot.self_following - bot.self_follower < 400:
                while len(bot.user_info_list) < 50:
                    feed_scanner(bot)
                    time.sleep(5 * 60)
                    follow_protocol(bot)
                    time.sleep(10 * 60)
                    check_status(bot)

        elif mode == 2:
            bot.bot_mode = 1
            bot.new_auto_mod()

        elif mode == 3:
            unfollow_protocol(bot)
            time.sleep(10 * 60)

        elif mode == 4:
            feed_scanner(bot)
            time.sleep(60)
            follow_protocol(bot)
            time.sleep(10 * 60)

        elif mode == 5:
            bot.bot_mode = 2
            unfollow_protocol(bot)

        else:
            print("Wrong mode!")


'''Return false if unable to login with creditials'''


def validate_account(username, password, proxy):
    bot = CustomBot(
        login=username,
        password=password,
        proxy=proxy,
        # sleep_at=-1,
        # sleep_time=1 * 60 * 60,
        # to_cleanup=False
    )
    status = bot.login_status
    bot.logout()

    return status
