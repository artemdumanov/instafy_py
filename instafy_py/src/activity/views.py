import json
import os
import signal
import time
from multiprocessing import Process

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponse
from django.http import HttpResponseServerError
from django.utils import timezone
from django.views.decorators.csrf import csrf_protect

from activity import controller
from models.models import InstagramAccount, Activity, Proxy
from utils import logging

log = logging.getLogger()


@login_required
@csrf_protect
def start_activity(request):
    try:
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)

        configs = None
        try:
            configs = body['configs']
        except KeyError as e:
            log.exception(e)
            raise

        username = request.session['current_account']
        account = InstagramAccount.objects.get(username=username)
        password = account.password
        proxy = Proxy.objects.get(account=account)

        log.debug('Start activity for {} with configs {}'.format(username,
                                                                 configs))

        # controller.create_bot(username, password)
        params = {}

        configs['instagram'] = account
        configs['username'] = username
        configs['password'] = password
        configs['proxy'] = proxy

        user_blacklist = {}

        try:
            if configs['user_blacklist']:
                for user in configs['user_blacklist']:
                    user_blacklist[user] = ''
        except KeyError as e:
            log.exception(e)
            raise

        try:
            for config in configs:
                value = configs[config]
                if (value is None) | (value == '') | (value == 'undefined'):
                    configs[config] = 0

            configs['user_blacklist'] = user_blacklist

            try:
                p = Process(target=controller.create_bot,
                            name=username,
                            kwargs=configs)
                p.start()
                # manager.add(p)
            except Exception as e:
                log.debug("Can't start activity {}"
                          .format(username))
                return HttpResponseServerError("Can't start activity {}"
                                               .format(username))

            configs.pop('instagram', None)
            configs.pop('password', None)
            configs.pop('proxy', None)

            activity = Activity()
            activity.instagram_account = InstagramAccount.objects.get(
                username=username)
            activity.configs = json.dumps(configs).replace('\'', '\"')
            activity.pid = p.pid
            activity.status = 'started'
            activity.creation_date = timezone.now()
            activity.save()

        except Exception as e:
            log.exception(e)
            raise

        log.info('Start activity for {}'.format(username))
        return HttpResponse("started")
    except Exception as e:
        log.exception(e)
        raise

@login_required
@csrf_protect
def get_activity_status(request):
    pid = request.GET.get('pid')


# TODO logging
@login_required
@csrf_protect
def check_activity(request):
    try:
        username = request.session['current_account']
        log.debug('Check activities of user: {}'.format(request.user))

        if 'current_account' in request.session:
            active_activity = Activity.objects.filter(
                Q(instagram_account=request.session['current_account'])) \
                .order_by('-creation_date')

            try:
                account = InstagramAccount.objects.get(username=request.session['current_account'])
            except InstagramAccount.DoesNotExist as e:
                log.debug('When check activity, found no accounts')
                return HttpResponse('none')
            else:
                today = timezone.now()

                if account.expired_date < today:
                    # what about stop all pid's here?
                    return HttpResponse('expired')

                if active_activity:
                    if active_activity.count() > 1:
                        log.info('Kill duplicated activities {} of account {}'
                                 .format(active_activity.values_list('id_activity'), username))
                        last_activity = active_activity[:1]
                        active_activity = active_activity[1:]
                        for activity in active_activity:
                            if activity.status == "started":
                                try:
                                    # if name is python
                                    os.kill(activity.pid, signal.SIGKILL)
                                except Exception as e:
                                    log.exception(e)
                                    raise
                                finally:
                                    activity.status = 'finished'
                                    activity.save()

                        last_activity = last_activity[0]
                        # last_activity.configs['status'] = last_activity.status

                        configs = json.loads(last_activity.configs)
                        configs['status'] = last_activity.status

                        return HttpResponse(json.dumps(configs), content_type='application/json')
                    elif active_activity.count() == 1:

                        configs = json.loads(active_activity[0].configs)

                        if 'user_blacklist' in configs:
                            configs['user_blacklist'] = [user for user in configs['user_blacklist']]
                        else:
                            configs['user_blacklist'] = []

                        # prevent if json value is absent
                        def fill_empty_value(value_list):
                            for item in value_list:
                                if not item in configs:
                                    configs[item] = []

                        fill_empty_value(['tag_list', 'tag_blacklist', 'comment_list',
                                          'user_blacklist'])
                        log.info('Pull activity: {}; after check'
                                 .format(active_activity[0].id_activity))

                        return HttpResponse(json.dumps(configs),
                                            content_type='application/json')
                else:
                    return HttpResponse('none')
        else:
            return HttpResponse('none')
    except Exception as e:
        log.exception(e)
        raise


@login_required
@csrf_protect
def stop_activity(request):
    try:
        if 'current_account' in request.session:
            active_activity = Activity.objects.filter(
                Q(instagram_account__username=request.session['current_account'])
                & Q(status='started'))
            log.info('Trying to stop activities of user: {}; active activities: {}'
                     .format(request.user, active_activity.values_list('pid', flat=True)))

            # TODO god, sorry
            # if, process was finished It can kill proccess of another user:
            if active_activity:
                for activity in active_activity:
                    try:
                        def kill():
                            os.kill(activity.pid, signal.SIGTERM)

                        # prevent vulture bug. When vulture is checking,
                        # and we trying to kill process, vulture will boot it again
                        log.info('Kill activity of user: {}; activity: {}'
                                 .format(request.user, activity.pid))
                        kill()

                        def p():
                            time.sleep(10)
                            log.info('Fuse kill activity of user: {}; activity: {}'
                                     .format(request.user, activity.pid))
                            kill()

                        Process(target=p).start()

                    except Exception as e:
                        log.exception(e, 'Can\'t kill activity of user: {}; '
                                         'activity: {}'.format(request.user,
                                                               activity.pid))
                        raise
                    finally:
                        activity.status = 'finished'
                        activity.save(update_fields=['status'])

                        log.info('Save changes after kill activity of user: {}; '
                                 'activity: {}'.format(request.user, activity.pid))

                return HttpResponse('stopped')
            else:
                log.info('User have no activities to stop activity: {}'
                         .format(request.user))

                return HttpResponse('none')
        else:
            return HttpResponse('none')
    except Exception as e:
        log.exception(e)
        raise
