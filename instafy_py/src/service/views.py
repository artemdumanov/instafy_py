import json
from datetime import datetime, timedelta

from django import views
from django.conf.global_settings import DATETIME_INPUT_FORMATS
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.db.models import Q
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseForbidden
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect

from activity.controller import validate_account
from models.models import InstagramAccount, Account, Mode, Feedback, Activity, Proxy
from utils import logging
from utils.proxy import ProxyManager

log = logging.getLogger()
proxy_manager = ProxyManager()


# Create your views here.


class ServiceMain(views.View):
    @method_decorator(login_required(login_url='/signin/'))
    def get(self, request, *args, **kwargs):
        # TODO check statuses there, for better perfomance

        if 'current_account' not in request.session:

            accounts = get_instgram_accounts_list_of_user(request)

            accounts_content = json.loads(
                accounts.content.decode('utf-8'))['accounts']

            if accounts_content:
                request.session['current_account'] = accounts_content[0]

        response = render(request, "service.html")
        response.set_cookie(key='language', value=request.user.language)
        return response


def get_instgram_accounts_list_of_user(request):
    if request.user.is_authenticated():

        # so long to log in
        # try:
        #     acc = [account.username,
        #            controller.validate_account(username=account.username, password=account.password)]
        #     data.append(acc)
        # except BadSignature as ex:
        #     log.exception(ex)

        insta_accounts = InstagramAccount.objects.filter(account_id_account=request.user.id_account)

        data = []
        for account in insta_accounts:
            data.append(account.username)

        posts_serialized = json.dumps({"accounts": data})
        log.info('Get all instagrams of ' + request.user.email + " " + posts_serialized)

        return HttpResponse(posts_serialized)
    else:
        return HttpResponseForbidden()


@login_required
@csrf_protect
def get_accounts_info(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)

    if 'account_list' in body:
        accounts = body['account_list']

        response = {}

        i = 0
        for account in accounts:
            account = InstagramAccount.objects.get(username=account)

            expire = account.expired_date
            expired_date = '%d/%d/%d' % (expire.year, expire.month, expire.day)

            response[i] = {
                'username': account.username,
                'history_of_payments': account.history_of_payments,
                'expired_date': expired_date
            }
            i += 1

        response = json.dumps(response)

        return JsonResponse(response, safe=False)
    else:
        return HttpResponseBadRequest()


'''return 901 if account already in use'''
'''return 902 if account not in use and able to log in to account'''
'''return 903 if account not in use and unable to log in to account'''


@login_required
@csrf_protect
def create_instagram_account(request):
    if request.user.is_authenticated():
        username = ''
        password = ''
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        try:
            username = body['username']
            password = body['password']
            ip = body['ip']
        except KeyError as e:
            log.exception(e)
            raise
        try:
            instagrams_of_user = InstagramAccount.objects.get(
                username=username
            )

            if instagrams_of_user:
                return HttpResponse('901', content_type='text/plain')
        except InstagramAccount.DoesNotExist:
            # get ip unoptimized. If credentials is valid,
            # sends two requests instead of one
            random_proxy = Proxy.objects.get(country=proxy_manager.get_country(ip)) \
                .order_by('?').first().proxy
            if validate_account(username=username,
                                password=password,
                                proxy=random_proxy):
                # signer = Signer(salt='extra')
                instagram_account = InstagramAccount()

                instagram_account.username = username
                instagram_account.account_id_account = Account.objects.get(
                    id_account=request.user.id_account)
                # instagram_account.password = signer.sign(password)
                instagram_account.password = password
                request.session['current_account'] = username

                account = request.user
                instagrams_count = InstagramAccount.objects. \
                    filter(account_id_account=account).count()

                trial = json.loads(account.trial)
                start_trial, end_trial = trial
                end_trial = datetime.strptime(end_trial, '%Y/%m/%d')

                tz = timezone.now()
                instagram_account.expired_date = tz + timedelta(days=3)
                instagram_account.save()

                log.info('Created a new instagram {} for account id: {}'
                         .format(instagram_account.username, account.id_account))
                log.debug('Created a new instagram {}'
                          .format(instagram_account))

                now = datetime.now()
                # if account trial and no accounts -> buy proxy
                if now < end_trial and instagrams_count == 0:
                    try:
                        # TODO subscription type, trial
                        proxy_manager.buy(instagram_account, ip,
                                          3, 'trial')
                    except Exception as e:
                        log.exception('Something wrong with proxy')
                        return HttpResponse('903', content_type='text/plain')
                else:
                    # prevent request to db
                    # if trial expired add account to  lst = 'expired_trial_accounts'
                    # if acc in lst show pricing
                    # session['expired_trial_accounts'].remove(username)
                    # session = request.session
                    # key = 'expired_trial_accounts'
                    # if key in session:
                    #     if session[key] != {}:
                    #         session[key].update(instagram_account.username)
                    #     else:
                    #         session[key] = {instagram_account.username}
                    #
                    # else:
                    #     session[key] = {instagram_account.username}
                    instagram_account.expired_date = tz - timedelta(days=1)
                    instagram_account.save(update_fields=['expired_date'])

                message = render_to_string(
                    template_name='add-instagram-email.html',
                    context={
                        'instagram_username': username,
                    }
                )
                send_mail(
                    subject='Добавлен новый аккаунт {}'.format(username),
                    message=message,
                    html_message=message,
                    from_email='support@ugram.co',
                    recipient_list=[request.user.email],
                    fail_silently=True,
                )

                return HttpResponse('902', content_type='text/plain')

                # TODO ! check  pass
                # if acc.account_type ==  account.trial if >< (start, end):
                #     instagram_account.expired_date = unix epoch
                # else:
                #     instagram_account.expired_date = datetime.today() \
                #                                      + timedelta(days=3)
                # Your trial is over, to start new activity, choose your plan

            else:
                return HttpResponse('903', content_type='text/plain')

    else:
        return HttpResponseForbidden()


@login_required
@csrf_protect
def set_password_of_instagram_account(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)

    if 'username' and 'password' in body:
        username = body['username']
        password = body['password']

        account = get_object_or_404(InstagramAccount, username=username)

        if account.password == password:
            return HttpResponse('passwords same')

        if account.account_id_account.email != request.user.email:
            return HttpResponseBadRequest()

        proxy = Proxy().objects.get(account=account).proxy
        if validate_account(username, password, proxy):

            account.password = password
            account.save(update_fields=['password'])

            return HttpResponse('modified')
        else:
            return HttpResponse('login error')
    else:
        return HttpResponseBadRequest()


def set_current_account(request):
    if request.user.is_authenticated():
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        try:
            username = body['current_account']
        except KeyError as e:
            raise
        else:
            request.session['current_account'] = username
            return HttpResponse(username, content_type='text/plain')
    else:
        return HttpResponseForbidden()


@login_required
@csrf_protect
def get_all_modes(request):
    modes = Mode.objects.filter(Q(id_mode=1) | Q(account_id_account__id_account=request.user.id_account))

    if modes:
        # modes_list = list(modes.values())
        modes_list = []

        for mode in modes:
            item = []
            item.append(mode.id_mode)
            item.append(mode.mode_name)
            modes_list.append(item)

        posts_serialized = json.dumps({"modes": modes_list})
        # print(posts_serialized)

        return JsonResponse(posts_serialized, safe=False)

    return HttpResponse(status=204)


@login_required
@csrf_protect
def add_mode(request):
    pass


@login_required
@csrf_protect
def check_instagram_account(request):
    log.debug('Staring check instagram')
    if 'current_account' in request.session:
        username = request.session['current_account']
        try:
            account = InstagramAccount.objects.get(username=username)
        except InstagramAccount.DoesNotExist as e:
            log.debug('When check instagram, found no accounts')
            return HttpResponse('none')
        else:
            is_account_expired = account.expired_date > timezone.now()
            runningActivities = Activity.objects.filter(Q(instagram_account=account)
                                                        & Q(status='started'))
            if runningActivities:
                runningActivities = True
            else:
                runningActivities = False

            # save data in session like {username: exp_date}
            # if exp - now >= 30m return true
            def account_last_login_expired():
                session = request.session
                if 'accounts_expiry' not in session \
                        or session['accounts_expiry'] is None:
                    session['accounts_expiry'] = {}
                    request.session.modified = True

                accounts = session['accounts_expiry']
                current_account = session['current_account']

                if current_account in accounts:

                    if (datetime.now() - datetime.strptime(
                            accounts[current_account],
                            DATETIME_INPUT_FORMATS[1])) \
                            .total_seconds() / 60.0 > 30:

                        accounts[current_account] = str(datetime.now())
                        request.session.modified = True

                        log.debug('Instagram account {} is not checked {}'
                                  .format(current_account, str(datetime.now())))

                        return True
                    else:
                        log.debug('Instagram account {} already checked {}'
                                  .format(current_account, str(datetime.now())))
                        return False
                else:
                    accounts[current_account] = str(datetime.now())
                    request.session.modified = True
                    log.debug('Instagram account {} already checked {}'
                              .format(current_account, str(datetime.now())))
                    return True

            # set last login cookie, cuz every request try to sign in, prevent ban
            # if cookie > 30 refresh login status else None
            login_status = None
            if is_account_expired:
                if account_last_login_expired():
                    proxy = Proxy.objects.get(account=account).proxy
                    try:
                        login_status = validate_account(account.username,
                                                        account.password,
                                                        proxy)
                    except Exception as e:
                        raise
                else:
                    login_status = True
            else:
                login_status = False

            response = {
                'expireStatus': is_account_expired,
                'loginStatus': login_status,
                'runningActivities': runningActivities
            }

            response = json.dumps(response)

            log.info('Check instagram; username: %s, data: %s' % (account.username,
                                                                  response))
            resp = JsonResponse(response, safe=False)
            return resp
    else:
        return JsonResponse('{}', safe=False)


@login_required
@csrf_protect
def add_feedback(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    feedback = Feedback()

    try:
        feedback.feedback = body['feedback']
    except KeyError as e:
        raise

    feedback.account = Account.objects.get(email=request.user)
    feedback.date = timezone.now()
    feedback.type = 'feedback'
    feedback.save()
    return HttpResponse('ok')


@login_required
@csrf_protect
def add_report(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    feedback = Feedback()

    try:
        feedback.feedback = body['report']
    except KeyError as e:
        raise

    feedback.account = Account.objects.get(email=request.user)
    feedback.date = timezone.now()
    feedback.type = 'report'
    feedback.save()
    return HttpResponse('ok')


@csrf_protect
@login_required
def get_instagram_accounts_expires(request):
    accounts = InstagramAccount.objects.filter(account_id_account__email=request.user)

    # for account in accounts:
    #    parse json [username, expires]
    # return HttpResponse(json)


@csrf_protect
@login_required
def change_instagram_password(request):
    """
    :param request:
    :return: <Return 901 if Old and new passwords match;
             Return 902 if Password was changed;
             Return 903 if Passwords don't match;>
    """
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)

    if 'username' and 'old_password' and 'new_password' in body:
        old_password = body['old_password']
        username = body['username']

        instagram = InstagramAccount.objects.get(username=username)
        current_password = instagram.password

        if current_password == old_password:
            new_password = body['new_password']
            if old_password == new_password:
                return HttpResponse('901')

            instagram.password(new_password)
            instagram.save(update_fields=['password'])

            return HttpResponse('902')
        else:
            return HttpResponse('903')
    else:
        return HttpResponseBadRequest()


def set_language(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)

    if 'language' in body:
        language = body['language']

        account = Account.objects.get(email=request.user)
        account.language = language
        account.save(update_fields=['language'])

        log.debug('Change language to {} of {}'.format(language,
                                                       account.email))
        return HttpResponse('OK')
    else:
        return HttpResponseBadRequest()


def email(request):
    template_name = 'add-instagram-email.html'
    welcome_email = render_to_string(
        template_name=template_name,
        context={'email': 'dymanovvv@gmail.com'}

    )
    if request.GET.get('s', None) is not None:
        send_mail(
            subject='К сожалению, ваша подписка закончилась',
            message=welcome_email,
            html_message=welcome_email,
            from_email='support@ugram.co',
            recipient_list=[request.user.email],
            fail_silently=False,
        )
    return render(request, template_name)
