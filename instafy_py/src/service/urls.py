from django.conf.urls import url

from service import views

urlpatterns = [
        url(r'^app/$', views.ServiceMain.as_view()),
        url(r'account/add/$', views.create_instagram_account, name="create_instagram_account"),
        url(r'account/set/$', views.set_current_account, name="set_current_account"),
        url(r'account/check/$', views.check_instagram_account, name="check_instagram_account"),
        url(r'account/password/set$', views.set_password_of_instagram_account, name="set_password_of_instagram_account"),
        url(r'account/language/set$', views.set_language, name="set_language"),
        url(r'accounts/$', views.get_instgram_accounts_list_of_user, name="get_instgram_accounts_list_of_user"),
        url(r'accounts/info/$', views.get_accounts_info, name="get_accounts_info"),
        url(r'feedback/add/$', views.add_feedback, name="add_feedback"),
        url(r'report/add/$', views.add_report, name="add_report"),
        url(r'email/$', views.email),
]