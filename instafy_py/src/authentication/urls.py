from django.conf.urls import url

from authentication import views

urlpatterns = [
    url(r'signup/$', views.SignUp.as_view()),
    url(r'signin/$', views.SignIn.as_view()),
    url(r'signout/$', views.sign_out, name="sign_out"),
    url(r'isavailable/$', views.validate_username, name='validate_username'),
    url(r'password/change/$', views.change_password, name='change_password'),
]
