var app = angular.module('AuthApp', ['ngMaterial', 'ngMessages', 'ngAnimate', 'ngAria'])

app.controller('SignUpCtrl', function ($scope, $http, $window) {
    $scope.showHints = true;

    $scope.user = {
        email: "",
        password: ""
    };

    $scope.checkEmail = function (email) {
        console.log(email);
        $http.post('/isavailable/', {email: email}).then(function mySucces(response) {
            var available = response.data.is_taken;

            console.log("EMAIL TAKEN?:" + available);
            $scope.userForm.email.$error.taken = available;
            $scope.userForm.$error.taken = available;
            console.log("FORM VALID?:" + !available);

            if ($scope.userForm.password.$error == undefined) {
                $scope.userForm.$valid = false;
            } else {
                $scope.userForm.$valid = !available;
            }

            return available;
        }, function myError(response) {
            console.log('ERROR:' + response)
        });
    };

    $scope.submit = function () {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "/captcha/check/signup/", false);
        function getCookie(name) {
            var cookie = " " + document.cookie;
            var search = " " + name + "=";
            var setStr = null;
            var offset = 0;
            var end = 0;
            if (cookie.length > 0) {
                offset = cookie.indexOf(search);
                if (offset != -1) {
                    offset += search.length;
                    end = cookie.indexOf(";", offset)
                    if (end == -1) {
                        end = cookie.length;
                    }
                    setStr = unescape(cookie.substring(offset, end));
                }
            }
            return (setStr);
        }

        xhr.setRequestHeader('X-CSRFToken', getCookie('csrftoken'))
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                resp = JSON.parse(xhr.responseText)

                if (resp.status) {
                    if ($scope.userForm.$valid && !$scope.userForm.email.$error.taken) {

                        $http.post('/signup/', {
                            email: $scope.userForm.email.$modelValue,
                            password: $scope.userForm.password.$modelValue
                        }).then(function mySucces(response) {
                            console.log(response)
                        }, function myError(response) {
                            console.log('ERRORs:' + JSON.stringify(response))
                        });

                        window.location = "/signin/";
                    } else {

                        return;
                    }
                }
            }
        };
        xhr.send('{\"response\":\"' + grecaptcha.getResponse(captcha) + "\"}")


    }

});
app.controller('SignInCtrl', function ($scope, $http, $window) {

});

app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);