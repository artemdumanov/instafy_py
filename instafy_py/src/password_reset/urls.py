from django.contrib.auth import views as auth_views
from django.conf.urls import url
from django.conf.urls import include

urlpatterns = [
    url('^', include('django.contrib.auth.urls')),
]