from django.core.mail import send_mail

from main.settings import EMAIL_HOST_USER, ADMIN_EMAILS


def mail_admin(message, subject=None):
    send_mail(
        subject,
        message,
        EMAIL_HOST_USER,
        ADMIN_EMAILS,
        fail_silently=False
    )